﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MouseAimCamera : MonoBehaviour
{	
	// Elementos del personaje
	public PlayerConfiguration playerConfig;
	public GameObject enfoque;
	public GameObject target;
	private CharacterController targetCharControl;
	private PlayerStatus playerStatus;
	private CharControl charControl;
	private Camera camera_comp;

	// Parámetros de la cámara
	public float rotateSpeed = 2.5f;
	public float upDownSpeed = 1.0f;
	public float aimSpeedCoef = 0.5f;
	public float cameraAim_DistanceThreshold = 0.2f;

	// Layers que la cámara no debe atravesar y Culling para la primera persona
	public LayerMask LayersToAvoidCameraGoThrough;
	public LayerMask CullingMaskTPS_ExceptLayers;
	public LayerMask CullingMaskFPS_ExceptLayers;

	public LayerMask CullingMaskTPS_ExceptLayers_inverted;
	public LayerMask CullingMaskFPS_ExceptLayers_inverted;
	private GameObject weapon;


	// Modos de cámara
	private const int FirstPerson = 0;
	private const int ThirdPersonDynamic = 1;
	private const int ThirdPersonFixed = 2;
	private int CameraMode;
	bool cameraFixed;

	// Posiciones de la cámara respecto del personajes
	private string playerClass;
	//private Vector3 ThirdPersonOffset = new Vector3(0.0f, -4.08f, 3.46f);
	//private Vector3 FirstPersonOffset = new Vector3(0.2f, -2.40f, -0.25f);
	//private Vector3 ThirdPersonAimOffset = new Vector3(-0.35f, -2.35f, 0.6f);
	//private Vector3 offset;
	private Dictionary<string, Vector3> ThirdPersonOffset = new Dictionary<string, Vector3>();
	private Dictionary<string, Vector3> FirstPersonOffset = new Dictionary<string, Vector3>();
	private Dictionary<string, Vector3> ThirdPersonAimOffset = new Dictionary<string, Vector3>();
	//public Vector3 FirstPersonOffsetTest = new Vector3(0.2f, -2.25f, -0.90f);
	private Dictionary<string, string> WeaponGameObjectRoute = new Dictionary<string, string>();
	private Dictionary<string, string> CharGameObjectRoute = new Dictionary<string, string>();

	// Variables
	private float angRotacion = 150.0f;
	private float camRotationY;
	private float camRotationX;
	private float camUpDown;
	private bool resetAction;
	[HideInInspector] public bool contPressed;
	private float resetCamPos;
	private bool lastFrameCollisionWall;

	private float manualCameraTimeReset = 1.5f;
	private float manualCameraTime;

	private bool aimZoomStarted;
	private bool aimZoomFinished;

	private bool playerDead;

	void initCameraOffsets ()
	{
		ThirdPersonOffset.Add("Explorer", new Vector3(0.0f, -4.08f, 3.46f));
		ThirdPersonOffset.Add("Mercenary", new Vector3(0.0f, -4.08f, 3.46f));
		ThirdPersonOffset.Add("Rude", new Vector3(0.0f, -4.08f, 3.46f));
		ThirdPersonOffset.Add("Soldier", new Vector3(0.0f, -4.08f, 3.46f));
		ThirdPersonOffset.Add("Speedy", new Vector3(0.0f, -4.08f, 3.46f));

		FirstPersonOffset.Add("Explorer", new Vector3(0.15f, -2.25f, -0.20f));
		FirstPersonOffset.Add("Mercenary", new Vector3(-0.07f, -2.25f, -0.20f));
		FirstPersonOffset.Add("Rude", new Vector3(0.30f, -2.50f, -0.20f));
		FirstPersonOffset.Add("Soldier", new Vector3(0.20f, -2.40f, -0.20f));
		FirstPersonOffset.Add("Speedy", new Vector3(-0.07f, -2.05f, -0.30f));

		ThirdPersonAimOffset.Add("Explorer", new Vector3(-0.38f, -2.28f, 0.60f));
		ThirdPersonAimOffset.Add("Mercenary", new Vector3(-0.38f, -2.32f, 0.60f));
		ThirdPersonAimOffset.Add("Rude", new Vector3(-0.55f, -2.55f, 0.60f));
		ThirdPersonAimOffset.Add("Soldier", new Vector3(-0.35f, -2.35f, 0.60f));
		ThirdPersonAimOffset.Add("Speedy", new Vector3(-0.44f, -2.38f, 0.60f));

		//Debug.Log (playerClass);
	}

	void initGameObjectRoutes ()
	{
		WeaponGameObjectRoute.Add("Explorer", "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/weapon_SniperRifle");
		WeaponGameObjectRoute.Add("Mercenary", "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/weapon_smg");
		WeaponGameObjectRoute.Add("Rude", "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/weapon_MachineGun");
		WeaponGameObjectRoute.Add("Soldier", "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/weapon_MachineGun");
		WeaponGameObjectRoute.Add("Speedy", "Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/weapon_smg");

		CharGameObjectRoute.Add("Explorer", "MESH_Sniper");
		CharGameObjectRoute.Add("Mercenary", "MESH_Scout");
		CharGameObjectRoute.Add("Rude", "MESH_Gunner");
		CharGameObjectRoute.Add("Soldier", "MESH_Gunner");
		CharGameObjectRoute.Add("Speedy", "MESH_Infantry");
	}

    void Start()
	{
		targetCharControl = target.GetComponentInParent<CharacterController>();
		playerConfig = target.GetComponentInParent<PlayerConfiguration>();
		playerStatus = target.GetComponentInParent<PlayerStatus>();
		charControl = target.GetComponentInParent<CharControl>();
		playerClass = playerConfig.getPlayerClass ();
		camera_comp = GetComponent<Camera> ();

		// Máscaras para el culling especial de la vista en primera persona
		initGameObjectRoutes ();
		int layerTeamNumber = LayerMask.NameToLayer (playerConfig.getTeamAndTeamNumberLayer ());			
		GameObject characterModel = target.transform.Find (CharGameObjectRoute[playerClass]).gameObject;
		characterModel.layer = layerTeamNumber;

		weapon = target.transform.Find(WeaponGameObjectRoute[playerClass]).gameObject;
		weapon.layer = LayerMask.NameToLayer ("Weapon");

		CullingMaskFPS_ExceptLayers.value = (int)Mathf.Pow(2, layerTeamNumber);
		CullingMaskTPS_ExceptLayers_inverted = ~(CullingMaskTPS_ExceptLayers);
		CullingMaskFPS_ExceptLayers_inverted = ~(CullingMaskFPS_ExceptLayers);
		camera_comp.cullingMask = CullingMaskTPS_ExceptLayers_inverted;

		//Debug.Log ("player class: " + playerClass + " Layer name: " + playerConfig.getTeamAndTeamNumberLayer () + " Layer val: " + layerTeamNumber + " currentMask: " + CullingMaskFPS_ExceptLayers.value);
		//Debug.Log ("RutaWeapon: " + WeaponGameObjectRoute[playerClass]);
		//Debug.Log ("RutaChar: " + CharGameObjectRoute[playerClass]);

		// Inicializar
		CameraMode = ThirdPersonFixed;
		cameraFixed = true;
		initCameraOffsets ();

		camRotationY = target.transform.eulerAngles.y;
		camRotationX = 0.0f;
		camUpDown = 0.0f;

		resetAction = false;
		resetCamPos = 0.0f;
		manualCameraTime = 0.0f;
		aimZoomStarted = false;
		aimZoomFinished = false;
		lastFrameCollisionWall = false;
		playerDead = false;
	}

	void Update()
	{
		// Si el jugador muere, se resetea la cámara al reaparecer
		if (playerStatus.GetCurrentHealth () <= 0)
		{
			playerDead = true;
		} else if (playerStatus.GetCurrentHealth () > 0 && playerDead)
		{
			playerDead = false;
			camRotationY = charControl.getCharRotation();
			camRotationX = 0.0f;
			camUpDown = 0.0f;
			resetAction = false;
			resetCamPos = 0.0f;
			manualCameraTime = 0.0f;
			aimZoomStarted = false;
			aimZoomFinished = false;

			// Ponemos al personaje mirando en la misma dirección que la cámara para que se aplique en este frame
			target.transform.rotation = Quaternion.Euler(0, camRotationY, 0);
		}

		if (playerConfig.userControl.isChangeWeapon ())
		{
			switchCameraMode ();
		}

		camRotationY = normalizeAngle (camRotationY);
		float horizontal = playerConfig.userControl.getCameraX () * rotateSpeed * 25.0f * Time.deltaTime;
		float vertical = playerConfig.userControl.getCameraY () * upDownSpeed * 25.0f * Time.deltaTime;
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// FIRST PERSON CAMERA ///////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if (isFirstPersonCamera ())
		{
			vertical = playerConfig.userControl.getCameraY () * rotateSpeed * 25.0f * Time.deltaTime;
			if (playerConfig.userControl.isAiming ())
			{
				horizontal = horizontal * aimSpeedCoef;
				vertical = vertical * aimSpeedCoef;
			}

			camRotationY = camRotationY + horizontal;
			camRotationY = normalizeAngle (camRotationY);					
			if (camRotationX + vertical < 30.0f && camRotationX + vertical > -30.0f)
			{
				camRotationX += vertical;
			}

			transform.position = target.transform.position - (Quaternion.Euler (0, camRotationY, 0) * FirstPersonOffset[playerClass]);
			//transform.position = target.transform.position - (Quaternion.Euler (0, camRotationY, 0) * FirstPersonOffsetTest);
			transform.rotation = Quaternion.Euler (camRotationX, camRotationY, 0);

			// Ponemos al personaje mirando en la misma dirección que la cámara para que se aplique en este frame
			target.transform.rotation = Quaternion.Euler(0, camRotationY, 0);
		}
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// THIRD PERSON CAMERA ///////////////////////////////////////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		else if (isThirdPersonCamera ())
		{
			// Cámara al hombro al pulsar el botón de apuntar ////////////////////////////////////////////////////////////
			if (playerConfig.userControl.isAiming () && !playerConfig.userControl.isRunning () && !aimZoomFinished)
			{
				if (!aimZoomStarted)
				{
					target.transform.rotation = Quaternion.Euler(0, camRotationY, 0);
				}
				aimZoomStarted = true;

				Vector3 posObjetivo = target.transform.position - (Quaternion.Euler (0, camRotationY, 0) * ThirdPersonAimOffset[playerClass]);
				Vector3 difPos = transform.position - posObjetivo;
				float difRotX = transform.eulerAngles.x;
				if (difRotX > 180)
					difRotX = difRotX - 360;
				else if (difRotX < -180)
					difRotX = difRotX + 360;

				if (Mathf.Abs (difPos.magnitude) < cameraAim_DistanceThreshold)
				{
					aimZoomFinished = true;
					camRotationX = 0.0f;
				} else
				{
					float moveCameraCoef = 9.5f;
					transform.position = posObjetivo + difPos * (1.0f - Mathf.Clamp(moveCameraCoef * Time.deltaTime, 0.0f, 1.0f));
					transform.rotation = Quaternion.Euler (difRotX * (1.0f - Mathf.Clamp(moveCameraCoef * Time.deltaTime, 0.0f, 1.0f)), camRotationY, transform.eulerAngles.z);

					difPos = transform.position - posObjetivo;
					if (Mathf.Abs (difPos.magnitude) < cameraAim_DistanceThreshold)
					{
						aimZoomFinished = true;
						camRotationX = 0.0f;
					}
				}
				aimZoomFinished = true;
				camRotationX = 0.0f;
			} else if (playerConfig.userControl.isAiming () && !playerConfig.userControl.isRunning () && aimZoomFinished)
			{
				camRotationY = camRotationY + horizontal * aimSpeedCoef;
				camRotationY = normalizeAngle (camRotationY);
				vertical = playerConfig.userControl.getCameraY () * rotateSpeed * aimSpeedCoef * 25.0f * Time.deltaTime;
				if (camRotationX + vertical < 30.0f && camRotationX + vertical > -30.0f)
				{
					camRotationX += vertical;
				}

				transform.position = target.transform.position - (Quaternion.Euler (0, camRotationY, 0) * ThirdPersonAimOffset[playerClass]);
				transform.rotation = Quaternion.Euler (camRotationX, camRotationY, 0);

				// Ponemos al personaje mirando en la misma dirección que la cámara para que se aplique en este frame
				target.transform.rotation = Quaternion.Euler(0, camRotationY, 0);

			} else if (aimZoomStarted && !lastFrameCollisionWall)
			{
				aimZoomFinished = false;

				Vector3 posActual = transform.position;
				float rotXActual = transform.eulerAngles.x;

				Vector3 posObjetivo = target.transform.position - (Quaternion.Euler (0, camRotationY, 0) * ThirdPersonOffset[playerClass]);
				transform.position = posObjetivo;
				transform.LookAt (enfoque.transform);
				transform.rotation = Quaternion.Euler (transform.eulerAngles.x - 25.0f, transform.eulerAngles.y, transform.eulerAngles.z);

				Vector3 difPos = posActual - posObjetivo;
				float difRotX = rotXActual - transform.eulerAngles.x;
				if (difRotX > 180)
					difRotX = difRotX - 360;

				if (Mathf.Abs (difPos.magnitude) < cameraAim_DistanceThreshold)
				{
					aimZoomStarted = false;
					camUpDown = 0.0f;
				} else
				{
					float moveCameraCoef = 9.5f;
					transform.position = posActual - difPos * Mathf.Clamp(moveCameraCoef * Time.deltaTime, 0.0f, 1.0f);
					transform.rotation = Quaternion.Euler (rotXActual - difRotX * Mathf.Clamp(moveCameraCoef * Time.deltaTime, 0.0f, 1.0f), transform.eulerAngles.y, transform.eulerAngles.z);

					difPos = transform.position - posObjetivo;
					if (Mathf.Abs (difPos.magnitude) < cameraAim_DistanceThreshold)
					{
						aimZoomStarted = false;
						camUpDown = 0.0f;
					}
				}
			} else
			{
				// Botón de reseteo de cámara ////////////////////////////////////////////////////////////////////////////////
				if (playerConfig.userControl.isCameraReset () || resetAction)
				{
					if (!resetAction)
						resetCamPos = target.transform.eulerAngles.y;

					// Movimiento suave de la cámara
					float difAng = norm180Angle (resetCamPos - camRotationY);
					if (Mathf.Abs (difAng) > 0.8f)
					{
						resetAction = true;
						camRotationY += difAng * Mathf.Clamp(8.0f * Time.deltaTime, 0.0f, 1.0f);
					} else
					{
						resetAction = false;
						camRotationY = resetCamPos;
						manualCameraTime = 0.0f;					
					}

					if (Mathf.Abs (camUpDown) > 1.0f)
					{
						resetAction = true;
						camUpDown -= camUpDown * Mathf.Clamp(8.0f * Time.deltaTime, 0.0f, 1.0f);
					} else
					{
						camUpDown = 0.0f;
					}

					cameraFixed = true;
					camRotationY = normalizeAngle (camRotationY);
				} else
				{
					resetAction = false;
					if (isThirdPersonDynamicCamera () && !playerConfig.userControl.isKeyboard ())
					{
						cameraFixed = false;
					}
				}			
		
				// Mover la cámara manualmente ////////////////////////////////////////////////////////////////////////////////
				if (horizontal != 0 && !resetAction)
				{
					//Debug.Log("X: " + Input.GetAxis("Camera X") + " Y: " + Input.GetAxis("Camera Y"));
					camRotationY = camRotationY + horizontal;
					camRotationY = normalizeAngle (camRotationY);

					manualCameraTime = manualCameraTimeReset;
				}
				if (vertical != 0 && !resetAction)
				{
					if (camUpDown + vertical < 30.0f && camUpDown + vertical > -30.0f)
					{
						camUpDown += vertical;
					}
				}

				manualCameraTime = manualCameraTime - Time.deltaTime;
		
				// Si corremos, la cámara tenderá a ubicarse tras el personaje (sólo modo dinámico) ////////////////////////////
				if (!cameraFixed && !resetAction && manualCameraTime <= 0.0)
				{
					if (playerConfig.userControl.isMoving ())
					{
						float difAng = (charControl.getCharRotation() - camRotationY);
						if ((horizontal == 0) && (Mathf.Abs (difAng) <= angRotacion) || (Mathf.Abs (difAng) >= 360 - angRotacion))
						{
							if (difAng >= 360 - angRotacion)
							{
								if (camRotationY >= 360 - angRotacion)
									difAng = ((charControl.getCharRotation() + 360) - camRotationY);
								if (camRotationY <= angRotacion)
									difAng = (charControl.getCharRotation() - (camRotationY + 360));
							}

							difAng = norm180Angle (difAng);
							//Debug.Log("P: " + target.transform.eulerAngles.y + " " + charControl.getCharRotation() + " Cam: " + camRotationY + " " + normalizeAngle (camRotationY) + " Dif: " + difAng + " " + (difAng * Mathf.Clamp(0.0f, 1.0f, 1.0f * Time.deltaTime)) + " " + Time.deltaTime);
							//Debug.Log("camRotationY: " + camRotationY + " playerRotation: " + charControl.getCharRotation() + " diffAng: " + difAng + " " + (1.0f * Time.deltaTime) + " " + Mathf.Clamp(0.0f, 1.0f, 1.0f * Time.deltaTime) + " " + (camRotationY + difAng * Mathf.Clamp(0.0f, 1.0f, 1.0f * Time.deltaTime)));
							camRotationY = camRotationY + difAng * Mathf.Clamp(1.0f * Time.deltaTime, 0.0f, 1.0f);
							camRotationY = normalizeAngle (camRotationY);
						}
					}
				}

				// Transformación final ///////////////////////////////////////////////////////////////////////////////////////////////

				Vector3 position = target.transform.position - (Quaternion.Euler (0, camRotationY, 0) * ThirdPersonOffset[playerClass]);
				transform.position = new Vector3 (position.x, position.y + camUpDown * 0.1f, position.z);

				transform.LookAt (enfoque.transform);
				transform.rotation = Quaternion.Euler (transform.eulerAngles.x - 25.0f + camUpDown, transform.eulerAngles.y, transform.eulerAngles.z);
			}

				// Comprobar que la cámata no atraviese muros ///////////////////////////////////////////////////////////////////////////////////////
			RaycastHit hit;
			if (Physics.Linecast (enfoque.transform.position, transform.position, out hit, LayersToAvoidCameraGoThrough))
			{
				transform.position = new Vector3 (hit.point.x, hit.point.y, hit.point.z);
				lastFrameCollisionWall = true;
			} else
			{
				lastFrameCollisionWall = false;
			}
		}
		//Debug.Log("Cam: " + transform.eulerAngles.y + " " + camRotationY);
	}
	
	public float getCamRotation()
	{
		return camRotationY;
	}

	public bool isFirstPersonCamera()
	{
		return CameraMode == FirstPerson;
	}

	public bool isThirdPersonCamera()
	{
		return CameraMode == ThirdPersonFixed || CameraMode == ThirdPersonDynamic;
	}

	public bool isThirdPersonFixedCamera()
	{
		return CameraMode == ThirdPersonFixed;
	}

	public bool isThirdPersonDynamicCamera()
	{
		return CameraMode == ThirdPersonDynamic;
	}

	public string getAimState()
	{
		string state;

		if (playerConfig.userControl.isAiming () && !playerConfig.userControl.isRunning () && !aimZoomFinished)
		{
			state = "CameraZoomingIn";
		}
		else if (playerConfig.userControl.isAiming () && !playerConfig.userControl.isRunning () && aimZoomFinished)
		{
			state = "CameraZoomed";
		}
		else if (aimZoomStarted)
		{
			state = "CameraZoomingOut";
		}
		else
		{
			state = "CameraNotZoomed";
		}

		return state;
	}

	public void switchCameraMode()
	{
		if (CameraMode == FirstPerson)
		{
			if (playerConfig.userControl.isKeyboard ())
			{
				CameraMode = ThirdPersonFixed;
				cameraFixed = true;
			}
			else
			{
				CameraMode = ThirdPersonDynamic;
				cameraFixed = false;
			}
			camRotationY = target.transform.eulerAngles.y;
			camUpDown = 0.0f;
			camera_comp.cullingMask = CullingMaskTPS_ExceptLayers_inverted;
		}
		else if (CameraMode == ThirdPersonDynamic)
		{
			CameraMode = ThirdPersonFixed;
			cameraFixed = true;
		}
		else if (CameraMode == ThirdPersonFixed)
		{
			CameraMode = FirstPerson;
			cameraFixed = true;
			camRotationY = target.transform.eulerAngles.y;
			camRotationX = 0.0f;
			camera_comp.cullingMask = CullingMaskFPS_ExceptLayers_inverted;
		}
	}
	
	private float normalizeAngle(float angle)
	{
		angle = angle % 360;
		if(angle < 0) angle = 360 + angle;
		return angle;
	}

	private float norm180Angle(float angle)
	{
		angle = angle % 360;
		if     (angle <= -180) angle = angle + 360;
		else if(angle >   180) angle = angle - 360;
		return angle;
	}
}