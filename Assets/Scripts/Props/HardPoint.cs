﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HardPoint : MonoBehaviour {

	[HideInInspector]
    public PlayerConfiguration.PlayerTeam currentTeam;
	[HideInInspector]
    public float extractionStatus;
    public float extractionIncrement = 10f;
    public Image extractionSlider;
    public GameObject extractionPanel;
    public float range = 5.0f;
	public GameObject treasure;
	public GameObject treasureHolder;
	private GameObject treasureObj;
	public AudioSource enterClip;
	public AudioSource winClip;
	public ParticleSystem hardPointParticles;
    public NotificationSystem notificationSystem;

    public Color neutralColor;
    public Color redTeamColor;
    public Color blueTeamColor;
    public Color greenTeamColor;

    void Start()
    {
        currentTeam = PlayerConfiguration.PlayerTeam.NoTeam;
	    extractionStatus = 0;
	    treasureObj = GameObject.Instantiate(treasureHolder, new Vector3(transform.position.x, 0.0f, transform.position.z), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
	    treasureObj = GameObject.Instantiate(treasure, new Vector3(transform.position.x, 1.35f, transform.position.z), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
	    treasureObj.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
        extractionSlider.fillAmount = extractionStatus / 100.0f;
        extractionPanel.gameObject.SetActive(false);
    }

    void Update()
    {
        switch(currentTeam)
        {
            case PlayerConfiguration.PlayerTeam.BlueTeam:
                extractionStatus += extractionIncrement * Time.deltaTime;
                extractionSlider.color = blueTeamColor;
                hardPointParticles.startColor = blueTeamColor;
                break;
            case PlayerConfiguration.PlayerTeam.RedTeam:
                extractionStatus += extractionIncrement * Time.deltaTime;
                extractionSlider.color = redTeamColor;
                hardPointParticles.startColor = redTeamColor;
                break;
            case PlayerConfiguration.PlayerTeam.GreenTeam:
                extractionStatus += extractionIncrement * Time.deltaTime;
                extractionSlider.color = greenTeamColor;
                hardPointParticles.startColor = greenTeamColor;
                break;
            default:
                extractionSlider.color = neutralColor;
                hardPointParticles.startColor = neutralColor;
                break;
        }

        if (extractionStatus > 0)
            extractionPanel.gameObject.SetActive(true);

        if (extractionStatus >= 100)
        {
        	winClip.Play();
            // Concedemos el tesoro a un jugador aleatorio del equipo ganador
            Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, range);
            int i = 0;

            while (i < hitColliders.Length)
            {
                GameObject colliderObj = hitColliders[i].gameObject;
                if (colliderObj.tag == "Player" && colliderObj.GetComponent<PlayerStatus>().GetCurrentHealth() > 0)
                {
                    colliderObj.GetComponent<PlayerConfiguration>().HasTreasure = true;
                    break;
                }

                i++;
            }

            Global.readyToCatch = true;

	        notificationSystem.OpenAllNotificationPanelsWithText(currentTeam + " captured the treasure");
	        
            // Destroy Hard Point
            Destroy(treasureObj);
            extractionSlider.gameObject.SetActive(false);
            extractionPanel.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }

        extractionSlider.fillAmount = extractionStatus / 100.0f;
    }

    void FixedUpdate()
    {
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, range);
        int i = 0;

        List<int> teams;
        teams = new List<int>();

        while (i < hitColliders.Length)
        {
            GameObject colliderObj = hitColliders[i].gameObject;
            if (colliderObj.tag == "Player")
            {
                int team = (int)colliderObj.GetComponent<PlayerConfiguration>().team;
                if(!teams.Contains(team))
                    teams.Add(team);
            }
            
            i++;
        }

		PlayerConfiguration.PlayerTeam lastTeam = currentTeam;
        if (teams.Count == 0 || teams.Count > 1) currentTeam = PlayerConfiguration.PlayerTeam.NoTeam;
        else if (teams.Count == 1)
        {
	        if (currentTeam != (PlayerConfiguration.PlayerTeam)teams[0])
	        {
	        	enterClip.Play();
                notificationSystem.OpenAllNotificationPanelsWithText(((PlayerConfiguration.PlayerTeam)teams[0]).ToString() + " is extracting the treasure");
	        }
            currentTeam = (PlayerConfiguration.PlayerTeam)teams[0];

            if (currentTeam == PlayerConfiguration.PlayerTeam.BlueTeam)
                Global.blueFoundHardPoint = true;
            else
                Global.redFoundHardPoint = true;
        }
    }
}
