﻿using UnityEngine;
using System.Collections;

public class SawTrap : MonoBehaviour {

    public float damage;
    public PlayerConfiguration.PlayerTeam Team;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<Character>())
        {
            PlayerConfiguration.PlayerTeam enemyTeam = other.GetComponent<PlayerConfiguration>().team;
            
            if(enemyTeam != Team)
            {
                other.SendMessage("Hit", damage, SendMessageOptions.DontRequireReceiver);
            }
        }
    }
}
