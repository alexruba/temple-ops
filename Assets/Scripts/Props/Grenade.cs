﻿using UnityEngine;
using System.Collections;

public class Grenade : MonoBehaviour {

	public GameObject explosion;
	public AudioSource explosionClip;

    public float damage;
    public float radius;
    public float timeToExplode;

    void Start()
    {
        StartCoroutine(Explode());
    }

	IEnumerator Explode()
    {
        yield return new WaitForSeconds(timeToExplode);
        explosionClip.Play();

        Destroy(GameObject.Instantiate(explosion, transform.position, Quaternion.identity),2);

        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, radius);
        int i = 0;
        while (i < hitColliders.Length)
        {
        	if(!hitColliders[i].isTrigger)
            	hitColliders[i].SendMessage("Hit", damage, SendMessageOptions.DontRequireReceiver);
            i++;
        }
	    
        StartCoroutine(DestroyGrenade());
    }

    IEnumerator DestroyGrenade()
    {
        yield return new WaitForSeconds(0.65f);
        Destroy(gameObject);
    }
}
