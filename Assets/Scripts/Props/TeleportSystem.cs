﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TeleportSystem : MonoBehaviour {

    private static List<Teleport> teleports;

    void Start()
    {
        teleports = new List<Teleport>();
    }

    public static void CreateTeleport(GameObject obj, int team)
    {
        Teleport teleport = new Teleport(obj, team);
        teleports.Add(teleport);
    }

    public static Teleport GetTeleport(int team)
    {
        for(int i = 0; i < teleports.Count; ++i)
        {
            if (teleports[i].Team == team)
                return teleports[i];
        }

        return null;
    }

    public static bool ExistsTeleportInTeam(int team)
    {
        for (int i = 0; i < teleports.Count; ++i)
        {
            if (teleports[i].Team == team)
                return true;
        }

        return false;
    }

    public static Vector3 UseTeleport(int team)
    {
        for (int i = 0; i < teleports.Count; ++i)
        {
            if (teleports[i].Team == team)
            {
                Teleport teleport = teleports[i];
                Destroy(teleport.TeleportObject);
                teleports.RemoveAt(i);
                return teleport.TeleportObject.transform.position;
            }
        }

        return new Vector3(0.0f, 0.0f, 0.0f);
    }

    public static List<Teleport> GetTeleports()
    {
        return teleports;
    }
}
