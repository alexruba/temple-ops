﻿using UnityEngine;
using System.Collections;

public class Teleport {
    
    private int team;
    private GameObject teleportObject;

    public Teleport(GameObject obj, int t)
    {
        this.team = t;
        this.teleportObject = obj;
    }

    public int Team
    {
        get { return team; }
        set { team = value; }
    }

    public GameObject TeleportObject
    {
        get { return teleportObject; }
        set { teleportObject = value; }
    }
}
