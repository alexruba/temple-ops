﻿using UnityEngine;
using System.Collections;

public class Treasure : MonoBehaviour {
    
	void OnTriggerEnter(Collider other)
    {
        if(Global.readyToCatch)
        {
            if(other.gameObject.tag == "Player" && other.GetComponent<PlayerStatus>().GetCurrentHealth() > 0)
            {
                other.gameObject.GetComponent<PlayerConfiguration>().HasTreasure = true;
                gameObject.SetActive(false);
            }
        }
    }
}
