﻿using UnityEngine;
using System.Collections;

public class SmokeGrenade : MonoBehaviour {

    public GameObject smoke;
    public AudioSource smokeClip;

    public float radius;
    public float timeToExplode;
    public float smokeTime;


	// Use this for initialization
	void Start () {
        StartCoroutine(Smoke());
	}

    IEnumerator Smoke()
    {
        yield return new WaitForSeconds(timeToExplode);
        Destroy(GameObject.Instantiate(smoke, transform.position, Quaternion.identity), smokeTime);
        smokeClip.Play();
        Destroy(gameObject, smokeTime);
    }
}
