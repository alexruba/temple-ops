﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ExtractionPoint : MonoBehaviour
{
    public float range = 5.0f;
    public PlayerConfiguration.PlayerTeam team;
    private GameObject winPanel;
	
	private bool gameFinished = false;
	
    void Awake()
    {
        winPanel = GameObject.Find("WinPanel");
    }

	void Start()
	{
		gameFinished = false;
		winPanel.SetActive (false);
	}
	
	void Update()
	{
        if (gameFinished) StartCoroutine(EndGame());
	}

    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(5);

        if(PhotonNetwork.offlineMode)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            GameObject.Find("PlayerNetwork").GetComponent<PhotonView>().RPC("RPC_LeaveGame", PhotonTargets.All);
        }
    }

    void FixedUpdate()
    {
        Collider[] hitColliders = Physics.OverlapSphere(gameObject.transform.position, range);
        int i = 0;
        
        while (i < hitColliders.Length)
        {
            GameObject colliderObj = hitColliders[i].gameObject;            

            if (colliderObj.tag == "Player" &&
                colliderObj.GetComponent<PlayerStatus>().GetCurrentHealth() > 0 &&
                colliderObj.GetComponent<PlayerConfiguration>().team == team && 
                colliderObj.GetComponent<PlayerConfiguration>().HasTreasure)
            {
                winPanel.GetComponentInChildren<Text>().text = team.ToString() + " wins!";
	            winPanel.SetActive(true);
	            gameFinished = true;
                break;
            }
            else if(colliderObj.tag == "Player" &&
                    colliderObj.GetComponent<PlayerStatus>().GetCurrentHealth() > 0 &&
                    colliderObj.GetComponent<PlayerConfiguration>().team == team)
            {
                if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
                    Global.BlueExtractionPointDiscovered = true;
                else
                    Global.RedExtractionPointDiscovered = true;
            }

            i++;
        }
    }
}
