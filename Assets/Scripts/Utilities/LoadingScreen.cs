﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour {
	
	public GameObject background;
	public GameObject text;
	public GameObject[] toHide;
	//public Slider slider;
	public Text percentage;
	
	void Start () {
		background.SetActive (false);
		text.SetActive (false);
		percentage.gameObject.SetActive (false);
	}
	
	public void OnLevelLoad (string level)
	{
		if (level == "Split2") {
			Global.numberLocalPlayers = 2;
		} else if (level == "Split4") {
			Global.numberLocalPlayers = 4;
		}

		if (level == "SelectSceneDebug") {
			PhotonNetwork.offlineMode = true;
		}
		StartCoroutine (DisplayLoadingScreen (level));
	}

    public void ShowScreen()
    {
        StartCoroutine(WaitFor());
    }
	
	IEnumerator DisplayLoadingScreen(string levelToLoad)
	{
		foreach (GameObject g in toHide)
			g.SetActive (false);
		background.SetActive (true);
		text.SetActive (true);
		percentage.gameObject.SetActive (true);
		percentage.text = "0%";
		
		AsyncOperation async = SceneManager.LoadSceneAsync (levelToLoad);
		while (!async.isDone) {
			percentage.text = ((int)(async.progress * 100)).ToString() + "%";
			yield return null;
		}
		
		yield return null;
	}

    IEnumerator WaitFor()
    {
        foreach (GameObject g in toHide)
            g.SetActive(false);
        background.SetActive(true);
        text.SetActive(true);

        while (!Global.mapReady) {
            yield return null;
        }

        yield return null;
    }
}