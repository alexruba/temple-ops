﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectDestroyer : MonoBehaviour {


    public List<string> objects;

	void Start () {
	
        for(int i = 0; i < objects.Count; i++)
        {
            Destroy(GameObject.Find(objects[i]));
        }
	}
}
