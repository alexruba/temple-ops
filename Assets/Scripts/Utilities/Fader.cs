﻿using UnityEngine;
using System.Collections;

public class Fader : MonoBehaviour {

    public Color fadeColor;
    public float fadeTime;
    public GameObject panel;
    public bool hideOnFade;
    private Color color;

	// Use this for initialization
	void Start () {
        panel.GetComponent<UnityEngine.UI.Image>().color = fadeColor;
        color = fadeColor;
        StartCoroutine("FadeIn");
	}

    IEnumerator FadeIn()
    {
        float elapsedTime = 0.0f;
        while (elapsedTime < fadeTime)
        {
            yield return new WaitForEndOfFrame();
            elapsedTime += Time.deltaTime;
            color.a = 1.0f - Mathf.Clamp01(elapsedTime / fadeTime);
            panel.GetComponent<UnityEngine.UI.Image>().color = color;
        }

        if (hideOnFade)
            panel.gameObject.SetActive(false);
    }
}
