﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class TeamSetup : MonoBehaviour {

    [SerializeField]
	public List<GameObject> teamPanels;
    [SerializeField]
	public List<GameObject> classPanels;
    [SerializeField]
	public List<GameObject> waitingPanels;
    [SerializeField]
	public List<GameObject> readyPanels;
    [SerializeField]
	public List<Image> selectionImages;
    [SerializeField]
	public Color blueTeamColor;
    [SerializeField]
	public Color redTeamColor;
    [SerializeField]
	public Color readyColor;
    [SerializeField]
	public Sprite soldierImage;
    [SerializeField]
	public Sprite explorerImage;
	[SerializeField]
	public Sprite rudeImage;
	[SerializeField]
	public Sprite mercenaryImage;
	[SerializeField]
	public Sprite speedyImage;
    [SerializeField]
	public GameObject loadingImage;
    [SerializeField]
	public List<GameObject> backButtons;

	private List<TeamMember> members;

	public List<UserControl> userControls;

    public string sceneToLoad;

	private int readyMembers;

    private State[] usersState;

    private enum State
    {
        Waiting,
        TeamSelection,
        ClassSelection,
        Finished,
        Ready
    };

	int kbPlayerNum;

	void Start()
	{
		members = new List<TeamMember> ();
		readyMembers = 0;

		for (int i = 0; i < teamPanels.Count; ++i)
        {
			teamPanels [i].SetActive (false);
			classPanels [i].SetActive (false);
			readyPanels [i].SetActive (false);
			waitingPanels [i].SetActive (true);

			members.Add(new TeamMember());
		}

        usersState = new State[userControls.Count];
        for (int i = 0; i < userControls.Count; ++i)
        {
            usersState[i] = State.Waiting;
        }

    }

	void Update()
	{
        if (readyMembers == userControls.Count)
        {
            loadingImage.SetActive(true);
            if (sceneToLoad == "Split2")
                Global.numberLocalPlayers = 2;
            else if (sceneToLoad == "Split4")
                Global.numberLocalPlayers = 4;
            SceneManager.LoadScene(sceneToLoad);
        }

		for(int i = 0; i < userControls.Count; ++i)
		{
            if(userControls[i].isChangeWeapon())
            {
				ReturnToMenu ();
            }
            switch (usersState[i])
            {
                case State.Waiting:
                    if (userControls[i].isJumping()) // A
                    {
						if (userControls [i].controllerDevice == "Kb")
							kbPlayerNum = i;
					
                        members[i].number = (PlayerConfiguration.PlayerNumber)(i + 1);
                        WaitingTeamSelection(i);
                    }
                    break;

                case State.TeamSelection:
					if (userControls[i].isJumping()) // A
                    {
						TeamSelectionClassSelection(i, PlayerConfiguration.PlayerTeam.BlueTeam);
                    }
                    else if (userControls[i].isReloading()) // X
                    {
						TeamSelectionClassSelection(i, PlayerConfiguration.PlayerTeam.RedTeam);
                    }
                    else if (Input.GetButtonDown(userControls[i].controllerDevice + "Crouch"))
                    {
                        TeamSelectionWaiting(i);
                    }
                    break;
                case State.ClassSelection:
                    if (userControls[i].isJumping()) // A
                    {
						ClassSelectionFinished(i, GameObject.Find("ClassSelectionPanel" + (i + 1)).GetComponent<TeamSelectionHorizontal>().currentClass);
                    }
                    else if (Input.GetButtonDown(userControls[i].controllerDevice + "Crouch"))
                    {
                        ClassSelectionTeamSelection(i);
                    }
                    break;
                case State.Finished:
                    if (userControls[i].isJumping()) // A
                    {
                        FinishedReady(i);
                    }
                    else if (Input.GetButtonDown(userControls[i].controllerDevice + "Crouch"))
                    {
                        FinishedClassSelection(i);
                    }
                    break;
                case State.Ready:
                    Ready(i);
                    break;
            }
        }
	}

	public void GoBack()
	{	
		switch (usersState [kbPlayerNum])
		{
			case State.TeamSelection:
			TeamSelectionWaiting (kbPlayerNum);
				break;
			case State.ClassSelection:
			ClassSelectionTeamSelection (kbPlayerNum);
				break;
			case State.Finished:
			FinishedClassSelection (kbPlayerNum);
				break;
		}
	}

	public void ReturnToMenu()
	{
		Destroy(GameObject.FindObjectOfType<PlayerNetwork>().gameObject);
		GameObject.Find("Canvas").GetComponent<LoadingScreen>().OnLevelLoad("Menu");
	}

	public void TeamSelectionWaiting(int user)
    {
        waitingPanels[user].SetActive(true);
        teamPanels[user].SetActive(false);
        usersState[user] = State.Waiting;
        backButtons[user].SetActive(false);
    }

	public void WaitingTeamSelection(int user)
    {
        waitingPanels[user].SetActive(false);
        teamPanels[user].SetActive(true);
        usersState[user] = State.TeamSelection;
        backButtons[user].SetActive(true);
    }

	public void TeamSelectionClassSelection(int user, PlayerConfiguration.PlayerTeam team)
    {
		members [user].team = team;
        teamPanels[user].SetActive(false);
        classPanels[user].SetActive(true);
		classPanels[user].GetComponent<Image>().color = 
			team == PlayerConfiguration.PlayerTeam.BlueTeam ? blueTeamColor : redTeamColor;
        usersState[user] = State.ClassSelection;
    }

	public void ClassSelectionTeamSelection(int user)
    {
        teamPanels[user].SetActive(true);
        classPanels[user].SetActive(false);
        usersState[user] = State.TeamSelection;
    }

	public void ClassSelectionFinished(int user, PlayerConfiguration.PlayerClass c)
    {
		members[user].c = c;
        classPanels[user].SetActive(false);
        readyPanels[user].SetActive(true);
        readyPanels[user].GetComponent<Image>().color = members[user].team == PlayerConfiguration.PlayerTeam.BlueTeam ? blueTeamColor : redTeamColor;
		readyPanels [user].GetComponentInChildren<Text> ().text = userControls [user].controllerDevice == "Kb" ? "Press Space when Ready" : "Press A when ready";

		switch (members [user].c) {
		case PlayerConfiguration.PlayerClass.Explorer:
			selectionImages [user].sprite = explorerImage;
			break;
		case PlayerConfiguration.PlayerClass.Speedy:
			selectionImages [user].sprite = speedyImage;
				break;
		case PlayerConfiguration.PlayerClass.Soldier:
			selectionImages [user].sprite = soldierImage;
				break;
		case PlayerConfiguration.PlayerClass.Mercenary:
			selectionImages [user].sprite = mercenaryImage;
				break;
		case PlayerConfiguration.PlayerClass.Rude:
			selectionImages [user].sprite = rudeImage;
				break;
		}

        usersState[user] = State.Finished;
    }

	public void FinishedClassSelection(int user)
    {
        classPanels[user].SetActive(true);
        readyPanels[user].SetActive(false);
        usersState[user] = State.ClassSelection;
    }

	public void FinishedReady(int user)
    {
        TeamManager.AddMember(members[user].team, members[user].c, members[user].number);
        readyMembers++;
        usersState[user] = State.Ready;
        backButtons[user].SetActive(false);
    }

	public void Ready(int user)
    {
		readyPanels [user].GetComponentInChildren<Text> ().text = "Ready";
        readyPanels[user].GetComponent<Image>().color = readyColor;
    }

	public void ChooseTeam(int team)
	{
		TeamSelectionClassSelection (kbPlayerNum, (PlayerConfiguration.PlayerTeam) team);
	}

	public void ChooseClass(int c)
	{
		ClassSelectionFinished (kbPlayerNum, (PlayerConfiguration.PlayerClass) c);
	}

	private bool isKeyboard(int num)
	{
		return num == kbPlayerNum;
	}
}
