﻿using UnityEngine;
using System.Collections;

public class CursorScript : MonoBehaviour
{
    public Texture2D cursorTexture;
    public Texture2D cursorTextureDown;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    void Start()
    {
        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }

    void Update()
    {
        if (Input.GetButton("KbFire"))
            Cursor.SetCursor(cursorTextureDown, hotSpot, cursorMode);
        else
            Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
    }
}
