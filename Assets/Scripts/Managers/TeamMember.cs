﻿using UnityEngine;
using System.Collections;

public class TeamMember {

	public GameObject memberObj;
	public PlayerConfiguration.PlayerTeam team;
	public PlayerConfiguration.PlayerNumber number;
	public PlayerConfiguration.PlayerClass c;
	public int teamNumber;

}
