﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TeamManager : MonoBehaviour {
	
	public static List<TeamMember> RedTeam;
	public static List<TeamMember> BlueTeam;

	void Awake()
	{
		DontDestroyOnLoad (gameObject);

		RedTeam = new List<TeamMember> ();
		BlueTeam = new List<TeamMember> ();
	}

	public static void Spawn(TeamMember tm, Vector3 pos)
	{
		GameObject member = Instantiate (tm.memberObj, pos, Quaternion.identity) as GameObject;
		member.transform.SetParent(GameObject.Find (tm.team.ToString ()).transform);
		
		if(Global.numberLocalPlayers == 2)
		{
			if (tm.number == PlayerConfiguration.PlayerNumber.Split1) {
				member.GetComponentInChildren<Camera> ().rect = new Rect (0.0f, 0.0f, 0.5f, 1.0f);
			}
            else if (tm.number == PlayerConfiguration.PlayerNumber.Split2) {
                member.GetComponentInChildren<Camera>().rect = new Rect(0.5f, 0.0f, 0.5f, 1.0f);
            }
		}
		else if(Global.numberLocalPlayers == 4)
		{
			if (tm.number == PlayerConfiguration.PlayerNumber.Split1) {
				member.GetComponentInChildren<Camera> ().rect = new Rect (0.0f, 0.5f, 0.5f, 0.5f);
			}
			else if (tm.number == PlayerConfiguration.PlayerNumber.Split2) {
				member.GetComponentInChildren<Camera> ().rect = new Rect (0.5f, 0.5f, 0.5f, 0.5f);
			}
			else if (tm.number == PlayerConfiguration.PlayerNumber.Split3) {
				member.GetComponentInChildren<Camera> ().rect = new Rect (0.0f, 0.0f, 0.5f, 0.5f);
			}
			else if (tm.number == PlayerConfiguration.PlayerNumber.Split4) {
				member.GetComponentInChildren<Camera> ().rect = new Rect (0.5f, 0.0f, 0.5f, 0.5f);
            }
		}
		else
			member.GetComponentInChildren<Camera> ().rect = new Rect (0.0f, 0.0f, 0.0f, 0.0f);

		member.GetComponentInChildren<PlayerConfiguration> ().team = tm.team;
		member.GetComponentInChildren<PlayerConfiguration> ().playerNumber = tm.number;
		member.GetComponentInChildren<PlayerConfiguration> ().SetUserControl (tm.number);
		member.GetComponentInChildren<PlayerConfiguration> ().setTeamNumber (tm.teamNumber);
	}

	public static void AddMember(PlayerConfiguration.PlayerTeam team, PlayerConfiguration.PlayerClass c, PlayerConfiguration.PlayerNumber number)
	{
		TeamMember tm = new TeamMember ();
		tm.team = team;
		tm.number = number;

		if (c == PlayerConfiguration.PlayerClass.Soldier) {
			if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
				tm.memberObj = Resources.Load ("SoldierB", typeof(GameObject)) as GameObject;
			else if (team == PlayerConfiguration.PlayerTeam.RedTeam)
				tm.memberObj = Resources.Load ("SoldierR", typeof(GameObject)) as GameObject;
			
		} else if(c == PlayerConfiguration.PlayerClass.Explorer){
			if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
				tm.memberObj = Resources.Load ("ExplorerB", typeof(GameObject)) as GameObject;
			else if (team == PlayerConfiguration.PlayerTeam.RedTeam)
				tm.memberObj = Resources.Load ("ExplorerR", typeof(GameObject)) as GameObject;
			
		} else if(c == PlayerConfiguration.PlayerClass.Mercenary){
			if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
				tm.memberObj = Resources.Load ("MercenaryB", typeof(GameObject)) as GameObject;
			else if (team == PlayerConfiguration.PlayerTeam.RedTeam)
				tm.memberObj = Resources.Load ("MercenaryR", typeof(GameObject)) as GameObject;
			
		} else if(c == PlayerConfiguration.PlayerClass.Speedy){
			if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
				tm.memberObj = Resources.Load ("SpeedyB", typeof(GameObject)) as GameObject;
			else if (team == PlayerConfiguration.PlayerTeam.RedTeam)
				tm.memberObj = Resources.Load ("SpeedyR", typeof(GameObject)) as GameObject;
			
		} else if(c == PlayerConfiguration.PlayerClass.Rude){
			if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
				tm.memberObj = Resources.Load ("RudeB", typeof(GameObject)) as GameObject;
			else if (team == PlayerConfiguration.PlayerTeam.RedTeam)
				tm.memberObj = Resources.Load ("RudeR", typeof(GameObject)) as GameObject;
		}

		AddMember (tm);
	}

	public static void AddMember(TeamMember tm)
	{
		if (tm.team == PlayerConfiguration.PlayerTeam.RedTeam)
		{
			RedTeam.Add (tm);
			tm.teamNumber = RedTeam.Count;
		} else if (tm.team == PlayerConfiguration.PlayerTeam.BlueTeam)
		{
			BlueTeam.Add (tm);
			tm.teamNumber = BlueTeam.Count;
		}
	}
}
