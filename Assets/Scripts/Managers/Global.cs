﻿using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour {

	public static bool readyToCatch;
	public static bool mapReady;
    public static int mapRows;
	public static int mapCols;
	public static Texture2D mapTexture;
	public static Texture2D mapFogTexture;
	public static int numberLocalPlayers;

    public static Vector3 blueExplorerPos;
    public static Vector3 redExplorerPos;
    public static bool blueFoundHardPoint;
    public static bool redFoundHardPoint;
    public static Vector3 hardPointPos;
    public static bool RedExtractionPointDiscovered;
    public static Vector3 RedExtractionPointPos;
    public static bool BlueExtractionPointDiscovered;
    public static Vector3 BlueExtractionPointPos;

	public static bool offlineMode = false;


    void Start()
    {
        readyToCatch = false;
    }
}
