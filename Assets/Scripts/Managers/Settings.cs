﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Settings : MonoBehaviour {

    public Slider gameVolumeSlider;
    public Text gameVolumeText;

    private AudioListener audioListener;

	void Start () {
        gameVolumeSlider.value = AudioListener.volume;
        gameVolumeText.text = AudioListener.volume.ToString();
        gameVolumeSlider.onValueChanged.AddListener(delegate { GameVolumeValueChanged(); });
	}

    public void GameVolumeValueChanged()
    {
        gameVolumeText.text = (gameVolumeSlider.value * 100).ToString("0");
        AudioListener.volume = gameVolumeSlider.value;
    }
}
