﻿using UnityEngine;
using System.Collections;

public class UserControl : MonoBehaviour
{
	public string controllerDevice;
	private string gamepad;
	
	private bool shooting;		// RT - Left Click
    private bool running;       // Left Stick Button - Left Shift
	private bool aiming;		// LT - Right Click
	private bool special1;		// RB - E
	private bool special2;		// LB - Q
	
	private bool crouching;		// B Button - Ctrl
	private bool reloading; 	// X Button - R
	private bool jumping;		// A Button - Space
	private bool changeWeapon;	// Y Button - Middle Click
	
	private bool resetCamera;   // Right Stick Click - B

    private bool info;          // Back Button - TAB
	private bool pause; 		// Start Button - Escape
	private bool changeView;	// Back Button - V
	
	private float moveX;		// X Axis Left Stick - AD
	private float moveY;		// Y Axis Left Stick - WS
	private float cameraX;		// X Axis Right Stick - Horizontal Mouse Move
	private float cameraY;		// Y Axis Right Stick - Vertical Mouse Move
	
	private bool moving;
	private bool movingCamera;
	
	
	// Use this for initialization
	void Start () 
	{
		DontDestroyOnLoad (gameObject);
		gamepad = controllerDevice;
		
		shooting = false;
        running = false;
        aiming = false;
		special1 = false;
		special2 = false;
		crouching = false;
		reloading = false;
		jumping = false;
		changeWeapon = false;
		resetCamera = false;
		pause = false;
		changeView = false;
		
		moveX = 0.0f;
		moveY = 0.0f;
		cameraX = 0.0f;
		cameraY = 0.0f;
		
		//changeToKeyboard();
	}
	
	// Update is called once per frame
	void Update ()
	{
		// Shooting
		if (Input.GetAxis(controllerDevice + "Fire") > 0.5 || Input.GetButton(controllerDevice + "Fire"))
		{
			shooting = true;
		}
		else
		{
			shooting = false;
		}

        // Running
        if (Input.GetAxis(controllerDevice + "Run") > 0.5 || Input.GetButton(controllerDevice + "Run"))
        {
            running = true;
        }
        else
        {
            running = false;
        }

        // Aiming
        if (Input.GetButton(controllerDevice + "Aim"))
		{
			aiming = true;
		}
		else
		{
			aiming = false;
		}
		
		// Special1
		if (Input.GetButtonDown(controllerDevice + "Special1"))
		{
			special1 = true;
		}
		else
		{
			special1 = false;
		}
		
		// Special2
		if (Input.GetAxis(controllerDevice + "Special2") > 0.5 || Input.GetButtonDown(controllerDevice + "Special2"))
		{
			special2 = true;
		}
		else
		{
			special2 = false;
		}
		
		// Jump
		if (Input.GetButtonDown(controllerDevice + "Jump"))
		{
			jumping = true;
		}
		else
		{
			jumping = false;
		}
		
		// Reload
		if (Input.GetButtonDown(controllerDevice + "Reload"))
		{
			reloading = true;
		}
		else
		{
			reloading = false;
		}
		
		// Crouch
		if (Input.GetButton(controllerDevice + "Crouch"))
		{
			crouching = true;
		}
		else
		{
			crouching = false;
		}
		
		// Change Weapon
		if (Input.GetButtonDown(controllerDevice + "ChangeWeapon"))
		{
			changeWeapon = true;
		}
		else
		{
			changeWeapon = false;
		}
		
		// Reset Camera
		if (Input.GetButtonDown(controllerDevice + "ResetCamera"))
		{
			resetCamera = true;
		}
		else
		{
			resetCamera = false;
		}
		
		// Pause
		if (Input.GetButtonDown(controllerDevice + "Pause"))
		{
			pause = true;
		}
		else
		{
			pause = false;
		}

        // Info
        if (Input.GetButton(controllerDevice + "Info"))
        {
            info = true;
        }
        else
        {
            info = false;
        }

        // Change view
        if (Input.GetButtonDown(controllerDevice + "View"))
		{
			changeView = true;
		}
		else
		{
			changeView = false;
		}
		
		
		// Movement
		moveX = Input.GetAxis(controllerDevice + "Horizontal");
		moveY = Input.GetAxis(controllerDevice + "Vertical");
		if(moveX != 0 || moveY != 0)	moving = true;
		else							moving = false;
		
		// Camera
		cameraX = Input.GetAxis(controllerDevice + "CameraX");
		cameraY = Input.GetAxis(controllerDevice + "CameraY");
		if(cameraX != 0 || cameraY != 0)	movingCamera = true;
		else								movingCamera = false;
	}
	
	public void changeToKeyboard()
	{
		controllerDevice = "Kb";
	}
	
	public void changeToGamepad()
	{
		controllerDevice = gamepad;
	}
	
	public bool isKeyboard()
	{
		return (controllerDevice.Equals("Kb"));
	}
	
	public bool isShooting()
	{
		return shooting;
	}

    public bool isRunning()
    {
        return running;
    }

    public bool isAiming()
	{
		return aiming;
	}
	
	public bool isSpecial1()
	{
		return special1;
	}
	
	public bool isSpecial2()
	{
		return special2;
	}
	
	public bool isCrouching()
	{
		return crouching;
	}
	
	public bool isReloading()
	{
		return reloading;
	}
	
	public bool isJumping()
	{
		return jumping;
	}
	
	public bool isChangeWeapon()
	{
		return changeWeapon;
	}

    public bool isInfo()
    {
        return info;
    }
	
	public bool isPause()
	{
		return pause;
	}
	
	public bool isChangeView()
	{
		return changeView;
	}
	
	public bool isCameraReset()
	{
		return resetCamera;
	}
	
	public float getMoveX()
	{
		return moveX;
	}
	
	public float getMoveY()
	{
		return moveY;
	}
	
	public float getCameraX()
	{
		return cameraX;
	}
	
	public float getCameraY()
	{
		return cameraY;
	}
	
	public bool isMoving()
	{
		return moving;
	}
	
	public bool isCameraMoving()
	{
		return movingCamera;
	}
}