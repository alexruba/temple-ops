﻿using UnityEngine;
using System.Collections;

public class PlayerConfiguration : MonoBehaviour {

    public enum PlayerNumber { Single, Split1, Split2, Split3, Split4 };
    public enum PlayerTeam { RedTeam, BlueTeam, GreenTeam, NoTeam };
	public enum PlayerClass { Explorer, Mercenary, Soldier, Speedy, Rude };

	[HideInInspector] public UserControl userControl;
    public PlayerTeam team;
    public Camera cam;
    public GameObject characterClass;
	public PlayerClass playerClass;
    private ICharacter character;
    public PlayerNumber playerNumber;
	public int teamNumber;
    private bool hasTreasure;

    void Awake()
    {
        character = characterClass.GetComponent<ICharacter>();
        hasTreasure = false;
    }

	public void SetUserControl(PlayerConfiguration.PlayerNumber num)
	{
		userControl = GameObject.Find ("UserControl" + (int)num).GetComponent<UserControl>();
	}

    public ICharacter Character
    {
        get { return character; }
        set { character = value; }
    }

    public bool HasTreasure
    {
        get { return hasTreasure; }
        set { hasTreasure = value;  }
    }

	public string getPlayerClass()
	{
		string playerClass_res = "Soldier";

		if (playerClass == PlayerClass.Explorer)
		{
			playerClass_res = "Explorer";
		}
		else if (playerClass == PlayerClass.Mercenary)
		{
			playerClass_res = "Mercenary";
		}
		else if (playerClass == PlayerClass.Rude)
		{
			playerClass_res = "Rude";
		}
		else if (playerClass == PlayerClass.Soldier)
		{
			playerClass_res = "Soldier";
		}
		else if (playerClass == PlayerClass.Speedy)
		{
			playerClass_res = "Speedy";
		}

		return playerClass_res;
	}

	public void setTeamNumber(int num)
	{
		if (num <= 0)
			teamNumber = 1;
		else if (num > 4)
			teamNumber = 4;
		else
			teamNumber = num;
	}

	public int getTeamNumber()
	{
		return teamNumber;
	}

	public string getTeamAndTeamNumberLayer()
	{
		string TeamAndTeamNumber = "";
		int n_TeamNumber = teamNumber;
		if (!PhotonNetwork.offlineMode)
		{
			n_TeamNumber = 1;
		}

		if (team == PlayerTeam.BlueTeam)
			TeamAndTeamNumber = "CharacterB" + n_TeamNumber;
		else if (team == PlayerTeam.RedTeam)
			TeamAndTeamNumber = "CharacterR" + n_TeamNumber;
		else if (team == PlayerTeam.GreenTeam)
			TeamAndTeamNumber = "CharacterG" + n_TeamNumber;

		return TeamAndTeamNumber;
	}
}
