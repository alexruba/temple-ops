﻿using UnityEngine;
using System.Collections;

public class SoldierClass : Character
{
    [SerializeField]
	public GameObject grenade;
    [SerializeField]
	public GameObject campingObject;
    [SerializeField]
	public GameObject circleArea;

    private float passiveRange;
    private float grenadeForce;

    public SoldierClass()
    {
        grenadeForce = 100;
        attack = 4;
        defense = 100;
        spawnTime = 5;
        special2Time = 5;
        special1Units = 2;
        special2Units = 1;
        special2Range = 5;
		walkSpeed = 5;
		runSpeed = 10;
    }

    public override void castPassive()
    {
        if (GetComponent<PlayerStatus>().GetCurrentHealth() <= 0)
        {
            if (!GameObject.Find("Teleport " + name))
            {
				GameObject teleport;
				if (PhotonNetwork.offlineMode) {
					teleport = GameObject.Instantiate(campingObject, transform.position, campingObject.transform.rotation) as GameObject;
				} else {
					teleport = PhotonNetwork.Instantiate ("Teleport", new Vector3 (transform.position.x, 0.0f, transform.position.z), campingObject.transform.rotation, 0);
				}

                teleport.name = "Teleport " + name;
                TeleportSystem.CreateTeleport(teleport, (int)playerConfig.team);
            }
        }
    }

	public override void castSpecial1()
	{
        GetComponentInChildren<Animator>().SetTrigger("Grenade");
        GetComponent<ThrowObject>().Throw(grenade, grenadeForce, ThrowObject.GrenadeType.Explosive);
	}

	public override void castSpecial2()
	{
		GameObject refillArea;
		if (PhotonNetwork.offlineMode) {
			refillArea = GameObject.Instantiate(circleArea, new Vector3(transform.position.x, 0.2f, transform.position.z), Quaternion.identity) as GameObject;
		} else {
			refillArea = PhotonNetwork.Instantiate ("RefillArea", new Vector3 (transform.position.x, 0.2f, transform.position.z), Quaternion.identity, 0);
		}

        refillArea.transform.SetParent(transform);

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, special2Range);
		int i = 0;
		while (i < hitColliders.Length) {
			hitColliders[i].SendMessage("ReloadSpecial1", SendMessageOptions.DontRequireReceiver);
			i++;
		}
	}

    public override void stopSpecial2()
    {
        Debug.Log("Special2");
    }
}
