﻿using UnityEngine;
using System.Collections;

public class MercenaryClass : Character
{
	[SerializeField]
	public GameObject smoke;

	private float smokeForce;

	public MercenaryClass()
	{
		smokeForce = 100;
		attack = 30.0f;
		defense = 100;
		spawnTime = 5;
		special2Time = 5;
		special1Units = 2;
		special2Units = 1;
		special2Range = 5;
		walkSpeed = 5;
		runSpeed = 10;
	}

	public override void castPassive(){}

	public override void castSpecial1()
	{
		GetComponentInChildren<Animator>().SetTrigger("Grenade");
		GetComponent<ThrowObject>().Throw(smoke, smokeForce, ThrowObject.GrenadeType.Smoke);
	}

	public override void castSpecial2()
	{
		attack = 1000;
	}

	public override void stopSpecial2()
	{
		attack = 50;
	}
}
