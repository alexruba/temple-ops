﻿using UnityEngine;
using System.Collections;

public class SpeedyClass : Character {

	[SerializeField]
	public GameObject trapObj;

	public SpeedyClass()
	{
		attack = 2.5f;
		defense = 100;
		spawnTime = 2.5f;
		special2Time = 5;
		special1Units = 1;
		special2Units = 5;
		special2Range = 5;
		walkSpeed = 5;
		runSpeed = 10;
	}

	public override void castPassive(){}

	public override void castSpecial1()
	{
		GameObject trap;
		if (PhotonNetwork.offlineMode) {
			trap = GameObject.Instantiate(trapObj, transform.position, trapObj.transform.rotation) as GameObject;
		} else {
			trap = PhotonNetwork.Instantiate ("Trap" + (PhotonNetwork.player.GetTeam() == PunTeams.Team.blue ? "B" : "R"), new Vector3 (transform.position.x, 0.0f, transform.position.z), trapObj.transform.rotation, 0);
		}
	}

	public override void castSpecial2()
	{
		GetComponent<CharControl> ().runningSpeed = 20;
		GetComponent<CharControl> ().walkingSpeed = 10;
	}

	public override void stopSpecial2()
	{
		GetComponent<CharControl> ().runningSpeed = 10;
		GetComponent<CharControl> ().walkingSpeed = 5;
	}
}
