﻿using UnityEngine;
using System.Collections;

public class RudeClass : Character {

	[SerializeField]
	private GameObject shieldObj;

    [SerializeField]
    private GameObject teleportObj;

    public RudeClass()
	{
		attack = 2.5f;
		defense = 200;
		spawnTime = 2.5f;
		special2Time = 5;
		special1Units = 1;
		special2Units = 5;
		special2Range = 5;
		walkSpeed = 5;
		runSpeed = 10;
	}

	public override void castPassive()
	{
		Collider[] hitColliders = Physics.OverlapSphere(transform.position, special2Range);
		int i = 0;
		while (i < hitColliders.Length) {
			hitColliders[i].SendMessage("IncreaseHealth", SendMessageOptions.DontRequireReceiver);
			i++;
		}
	}

	public override void castSpecial1()
	{
        GameObject teleport;
        if (PhotonNetwork.offlineMode)
        {
            teleport = GameObject.Instantiate(teleportObj, transform.position, teleportObj.transform.rotation) as GameObject;
        }
        else
        {
            teleport = PhotonNetwork.Instantiate("Teleport", new Vector3(transform.position.x, 0.0f, transform.position.z), teleportObj.transform.rotation, 0);
        }

        teleport.name = "Teleport " + name;
        TeleportSystem.CreateTeleport(teleport, (int)playerConfig.team);
    }

	public override void castSpecial2()
	{
        shieldObj.SetActive (true);

		if (!PhotonNetwork.offlineMode) {
			ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
			hashTable.Add ("shield", GetComponent<PhotonView> ().viewID);
			PhotonNetwork.player.SetCustomProperties (hashTable);
		}
	}

	public override void stopSpecial2()
	{
        shieldObj.SetActive (false);

		if (!PhotonNetwork.offlineMode) {
			ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
			hashTable.Add ("unshield", GetComponent<PhotonView> ().viewID);
			PhotonNetwork.player.SetCustomProperties (hashTable);
		}
	}
}
