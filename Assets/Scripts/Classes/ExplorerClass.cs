﻿using UnityEngine;
using System.Collections;

public class ExplorerClass : Character
{
    [SerializeField]
	public GameObject particlesDisappear;
    [SerializeField]
	public GameObject smoke;

    private float smokeForce;

    public ExplorerClass()
    {
        smokeForce = 100;
        attack = 75;
        defense = 100;
        spawnTime = 5;
        special2Time = 5;
        special1Units = 2;
        special2Units = 1;
        special2Range = 5;
		walkSpeed = 5;
		runSpeed = 10;
    }

    public override void castPassive()
    {
        if (playerConfig.team == PlayerConfiguration.PlayerTeam.BlueTeam)
            Global.blueExplorerPos = transform.position;
        else
            Global.redExplorerPos = transform.position;
    }

    public override void castSpecial1()
    {
        GetComponentInChildren<Animator>().SetTrigger("Grenade");
        GetComponent<ThrowObject>().Throw(smoke, smokeForce, ThrowObject.GrenadeType.Smoke);
    }

    public override void castSpecial2()
    {
		if (PhotonNetwork.offlineMode) {
			GameObject.Instantiate(particlesDisappear, transform.position, particlesDisappear.transform.rotation);
		} else {
			PhotonNetwork.Instantiate ("ParticlesDisappear", transform.position, particlesDisappear.transform.rotation, 0);

			ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
			hashTable.Add ("disappear", GetComponent<PhotonView>().viewID);
			PhotonNetwork.player.SetCustomProperties (hashTable);
		}

        for (int i = 0; i < GetComponentsInChildren<Renderer>().Length; ++i)
            GetComponentsInChildren<Renderer>()[i].enabled = false;
    }

    public override void stopSpecial2()
    {
        for (int i = 0; i < GetComponentsInChildren<Renderer>().Length; ++i)
            GetComponentsInChildren<Renderer>()[i].enabled = true;

		if (!PhotonNetwork.offlineMode) {
			ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
			hashTable.Add ("appear", GetComponent<PhotonView>().viewID);
			PhotonNetwork.player.SetCustomProperties (hashTable);
		}
    }
}
