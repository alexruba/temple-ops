﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class Menu : MonoBehaviour {

    public GameObject settingsPanel;

	void Start()
    {
        settingsPanel.SetActive(false);
    }

	public void LoadSplit2Scene(int scene)
	{
		PhotonNetwork.offlineMode = true;
		SceneManager.LoadScene (scene);
		Global.numberLocalPlayers = 2;
	}
	
	public void LoadSplit4Scene(int scene)
	{
		PhotonNetwork.offlineMode = true;
		SceneManager.LoadScene (scene);
		Global.numberLocalPlayers = 4;
	}

    public void OpenSettings()
    {
        settingsPanel.SetActive(true);
    }

    public void CloseSettings()
    {
        settingsPanel.SetActive(false);
    }

    public void ExitClick()
    {
        Application.Quit();
    }

	public void OnContinueButtonClick()
	{
		settingsPanel.SetActive (false);
	}

	public void OnSettingsButtonClick()
	{

	}

	public void OnExitGameButtonClick()
	{
		PhotonNetwork.LeaveRoom ();
		Application.Quit ();
	}

	public void OnBackToMenuButtonClick()
	{
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        Destroy (GameObject.Find ("PlayerNetwork"));
		PhotonNetwork.LeaveRoom ();
		PhotonNetwork.LoadLevel (0);
	}

	public void LoadRoomList()
	{
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        PhotonNetwork.offlineMode = false;
		SceneManager.LoadScene (1);
	}
}
