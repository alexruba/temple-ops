﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class MainCanvasManager : MonoBehaviour 
{
	public static MainCanvasManager Instance;

	[SerializeField]
	private LobbyCanvas _lobbyCanvas;
	public LobbyCanvas LobbyCanvas
	{
		get { return _lobbyCanvas; }
	}

	[SerializeField]
	private CurrentRoomCanvas _currentRoomCanvas;
	public CurrentRoomCanvas CurrentRoomCanvas
	{
		get { return _currentRoomCanvas; }
	}

	private void Awake()
	{
		Instance = this;
	}

	public void OnClickBackToMenu()
	{
		Destroy (GameObject.Find ("PlayerNetwork"));
		SceneManager.LoadScene (0);
		PhotonNetwork.Disconnect ();
	}
}
