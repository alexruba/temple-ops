﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerLayoutGroup : MonoBehaviour
{
	[SerializeField]
	private GameObject _playerListingPrefab;
	private GameObject PlayerListingPrefab
	{
		get { return _playerListingPrefab; }
	}

	[HideInInspector]
	public List<PlayerListing> PlayerListings = new List<PlayerListing>();

	public void RemoveAllPlayers()
	{
		foreach (Transform child in transform) {
			Destroy (child.gameObject);
		}
	}

	public void AddPlayerListing(PhotonPlayer photonPlayer)
	{
		GameObject playerListingObj = Instantiate (PlayerListingPrefab);
		playerListingObj.transform.SetParent (transform, false);

		PlayerListing playerListing = playerListingObj.GetComponent<PlayerListing> ();
		playerListing.ApplyPhotonPlayer (photonPlayer);

		PlayerListings.Add (playerListing);
	}

	public void RemovePlayerListing(PhotonPlayer photonPlayer)
	{
		int index = PlayerListings.FindIndex (x => x.PhotonPlayer == photonPlayer);

		if (index != -1) {
			Destroy(PlayerListings[index].gameObject);
			PlayerListings.RemoveAt (index);
		}
	}
}
