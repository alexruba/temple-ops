﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChooseClassNetwork : MonoBehaviour 
{
	[SerializeField]
	private Button[] _classButtons;
	private Button[] ClassButtons
	{
		get { return _classButtons; }
	}

	[HideInInspector]
	public PlayerConfiguration.PlayerClass PlayerClass;

	public void OnChooseClassClick()
	{
		UpdateButtonsState ();
	}

	public void OnClassClicked(int clickedClass)
	{
		PlayerConfiguration.PlayerClass currentClass = (PlayerConfiguration.PlayerClass)clickedClass;
		PlayerClass = currentClass;
		SetPhotonPlayerClass ();
		UpdateButtonsState();
	}

	private void SetPhotonPlayerClass()
	{
		ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
		hashTable ["class"] = PlayerClass;
		PhotonNetwork.player.SetCustomProperties (hashTable);
	}

	private void UpdateButtonsState()
	{
		for(int i = 0; i < ClassButtons.Length; ++i) {
			PlayerConfiguration.PlayerClass currentClass = (PlayerConfiguration.PlayerClass)i;
			if (currentClass == PlayerClass) {
				ClassButtons [i].interactable = false;
			} else {
				ClassButtons [i].interactable = true;
			}
		}
	}
}
