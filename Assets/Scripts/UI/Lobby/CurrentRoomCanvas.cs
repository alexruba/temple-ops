﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CurrentRoomCanvas : MonoBehaviour 
{
	[SerializeField]
	private Button _startButton;
	private Button StartButton
	{
		get { return _startButton; }
	}

	[SerializeField]
	private Button _redTeamButton;
	private Button RedTeamButton
	{
		get { return _redTeamButton; }
	}

	[SerializeField]
	private Button _blueTeamButton;
	private Button BlueTeamButton
	{
		get { return _blueTeamButton; }
	}

	[SerializeField]
	private Button _roomStateButton;
	private Button RoomStateButton
	{
		get { return _roomStateButton; }
	}

	[SerializeField]
	private GameObject _startErrorPanel;
	private GameObject StartErrorPanel
	{
		get { return _startErrorPanel; }
	}

	[SerializeField]
	private GameObject _startingGamePanel;
	private GameObject StartingGamePanel
	{
		get { return _startingGamePanel; }
	}

	void OnJoinedRoom()
	{
		ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
		hashTable ["class"] = PlayerConfiguration.PlayerClass.Explorer;
		PhotonNetwork.player.SetCustomProperties (hashTable);

		if (!PhotonNetwork.isMasterClient) {
			StartButton.interactable = false;
			StartButton.GetComponentInChildren<Text> ().text = "Wait...";
			RoomStateButton.interactable = false;
		} else {
			StartButton.interactable = true;
			StartButton.GetComponentInChildren<Text> ().text = "Start";
			RoomStateButton.interactable = true;
		}
	}

	public void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
	{
		if (propertiesThatChanged.ContainsKey ("open")) {
			RoomStateButton.GetComponentInChildren<Text> ().text = PhotonNetwork.room.IsOpen ? "Public" : "Private";
		}

		if (propertiesThatChanged.ContainsKey ("starting")) {
			StartingGamePanel.SetActive (true);
		}
	}

	public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps) 
	{
		PhotonPlayer player = playerAndUpdatedProps[0] as PhotonPlayer;
		ExitGames.Client.Photon.Hashtable props = playerAndUpdatedProps [1] as ExitGames.Client.Photon.Hashtable;

		if (!props.ContainsKey ("class") && player == PhotonNetwork.player) {
			if (player.GetTeam () == PunTeams.Team.red) {
				RedTeamButton.interactable = false;
				BlueTeamButton.interactable = true;
			} else if(player.GetTeam() == PunTeams.Team.blue) {
				RedTeamButton.interactable = true;
				BlueTeamButton.interactable = false;
			} else {
				BlueTeamButton.interactable = true;
				RedTeamButton.interactable = true;
			}
		}
	}

	public void OnClickStartSync()
	{
		if (!PhotonNetwork.isMasterClient)
			return;
		
		PhotonNetwork.LoadLevel (8);
	}

	public void OnClickStartDelayed()
	{
		if (!PhotonNetwork.isMasterClient)
			return;

		if (CanStartGame ()) {
			ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
			hashTable.Add ("starting", true);
			PhotonNetwork.room.SetCustomProperties (hashTable);

			PhotonNetwork.room.IsOpen = false;
			PhotonNetwork.room.IsVisible = false;
			PhotonNetwork.LoadLevel (8);
		} else {
			StartErrorPanel.SetActive (true);
		}
	}

	private bool CanStartGame()
	{
		PhotonPlayer[] photonPlayers = PhotonNetwork.playerList;

		if (photonPlayers.Length <= 1)
			return false;

		int blueTeamCount = 0;
		int redTeamCount = 0;

		foreach (PhotonPlayer photonPlayer in photonPlayers) {
			if (photonPlayer.GetTeam () == PunTeams.Team.red) {
				redTeamCount++;
			} else if (photonPlayer.GetTeam () == PunTeams.Team.blue) {
				blueTeamCount++;
			}
		}
		return true;
		//return redTeamCount != 0 && blueTeamCount != 0;
	}

	public void OnClickRoomState()
	{
		if (!PhotonNetwork.isMasterClient)
			return;
		
		PhotonNetwork.room.IsOpen = !PhotonNetwork.room.IsOpen;
		PhotonNetwork.room.IsVisible = PhotonNetwork.room.IsOpen;

		ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
		hashTable.Add ("open", PhotonNetwork.room.IsOpen);
		PhotonNetwork.room.SetCustomProperties (hashTable);
	}

	public void OnClickLeaveRoom()
	{
		PhotonNetwork.LeaveRoom ();
	}
}
