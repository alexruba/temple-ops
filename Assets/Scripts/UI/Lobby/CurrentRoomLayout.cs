﻿using UnityEngine;
using System.Collections;

public class CurrentRoomLayout : MonoBehaviour 
{
	[SerializeField]
	private PlayerLayoutGroup _blueTeamLayoutGroup;
	private PlayerLayoutGroup BlueTeamLayoutGroup
	{
		get { return _blueTeamLayoutGroup; }
	}

	[SerializeField]
	private PlayerLayoutGroup _redTeamLayoutGroup;
	private PlayerLayoutGroup RedTeamLayoutGroup
	{
		get { return _redTeamLayoutGroup; }
	}

	private void OnMasterClientSwitched(PhotonPlayer newMasterClient)
	{
		PhotonNetwork.LeaveRoom ();
	}

	private void OnJoinedRoom()
	{
		BlueTeamLayoutGroup.RemoveAllPlayers ();
		RedTeamLayoutGroup.RemoveAllPlayers ();

		MainCanvasManager.Instance.CurrentRoomCanvas.transform.SetAsLastSibling ();

		PhotonPlayer[] photonPlayers = PhotonNetwork.playerList;

		foreach (PhotonPlayer photonPlayer in photonPlayers) {
			PlayerJoinedRoom (photonPlayer);
		}
	}

	private void OnPhotonPlayerConnected(PhotonPlayer photonPlayer)
	{
		PlayerJoinedRoom (photonPlayer);
	}

	private void OnPhotonPlayerDisconnected(PhotonPlayer photonPlayer)
	{
		PlayerLeftRoom (photonPlayer);
	}

	private void PlayerJoinedRoom(PhotonPlayer photonPlayer)
	{
		if (photonPlayer == null)
			return;

		PlayerLeftRoom (photonPlayer);

		if (PhotonNetwork.player == photonPlayer) {
			PhotonNetwork.player.SetTeam (PhotonNetwork.player.GetTeam() == PunTeams.Team.red ? PunTeams.Team.blue : PunTeams.Team.red);
		} else {
			if (photonPlayer.GetTeam () == PunTeams.Team.blue) {
				BlueTeamLayoutGroup.AddPlayerListing (photonPlayer);
			} else if (photonPlayer.GetTeam () == PunTeams.Team.red) {
				RedTeamLayoutGroup.AddPlayerListing (photonPlayer);
			}
		}
	}

	private void PlayerLeftRoom(PhotonPlayer photonPlayer)
	{
		if (photonPlayer.GetTeam () == PunTeams.Team.blue) {
			BlueTeamLayoutGroup.RemovePlayerListing (photonPlayer);
		} else if (photonPlayer.GetTeam () == PunTeams.Team.red) {
			RedTeamLayoutGroup.RemovePlayerListing (photonPlayer);
		} else {
			BlueTeamLayoutGroup.RemovePlayerListing (photonPlayer);
			RedTeamLayoutGroup.RemovePlayerListing (photonPlayer);
		}
	}

	public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps) 
	{
		PhotonPlayer player = playerAndUpdatedProps[0] as PhotonPlayer;
		ExitGames.Client.Photon.Hashtable props = playerAndUpdatedProps [1] as ExitGames.Client.Photon.Hashtable;

		if (!props.ContainsKey("class")) {
			if (player.GetTeam () == PunTeams.Team.red) {
				RedTeamLayoutGroup.AddPlayerListing (player);
				BlueTeamLayoutGroup.RemovePlayerListing (player);
			} else if(player.GetTeam() == PunTeams.Team.blue) {
				BlueTeamLayoutGroup.AddPlayerListing (player);
				RedTeamLayoutGroup.RemovePlayerListing (player);
			}
		}
	}

	public void OnClickJoinTeam(int team)
	{
		PunTeams.Team teamToJoin = (PunTeams.Team) team;
		PhotonNetwork.player.SetTeam (teamToJoin);
	}
}
