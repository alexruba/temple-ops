﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerListing : MonoBehaviour {

	public PhotonPlayer PhotonPlayer { get; private set; }

	[SerializeField]
	private Text _playerName;
	private Text PlayerName
	{
		get { return _playerName; }
	}

	[SerializeField]
	private Image _playerClass;
	private Image PlayerClass
	{
		get { return _playerClass; }
	}

	[SerializeField]
	private Sprite _explorerImage;
	private Sprite ExplorerImage
	{
		get { return _explorerImage; }
	}

	[SerializeField]
	private Sprite _soldierImage;
	private Sprite SoldierImage
	{
		get { return _soldierImage; }
	}

	[SerializeField]
	private Sprite _rudeImage;
	private Sprite RudeImage
	{
		get { return _rudeImage; }
	}

	[SerializeField]
	private Sprite _speedyImage;
	private Sprite SpeedyImage
	{
		get { return _speedyImage; }
	}

	[SerializeField]
	private Sprite _mercenaryImage;
	private Sprite MercenaryImage
	{
		get { return _mercenaryImage; }
	}

	public void ApplyPhotonPlayer(PhotonPlayer photonPlayer)
	{
		PhotonPlayer = photonPlayer;
		PlayerName.text = photonPlayer.NickName;

		if (photonPlayer.CustomProperties.ContainsKey ("class")) {
			PlayerConfiguration.PlayerClass currentClass = (PlayerConfiguration.PlayerClass)photonPlayer.CustomProperties ["class"];
			UpdatePlayerClassImage (currentClass);
		}
	}

	private void UpdatePlayerClassImage(PlayerConfiguration.PlayerClass newClass)
	{
		switch (newClass) {
		case PlayerConfiguration.PlayerClass.Explorer:
			PlayerClass.sprite = ExplorerImage;
			break;
		case PlayerConfiguration.PlayerClass.Soldier:
			PlayerClass.sprite = SoldierImage;
			break;
		case PlayerConfiguration.PlayerClass.Mercenary:
			PlayerClass.sprite = MercenaryImage;
			break;
		case PlayerConfiguration.PlayerClass.Rude:
			PlayerClass.sprite = RudeImage;
			break;
		case PlayerConfiguration.PlayerClass.Speedy:
			PlayerClass.sprite = SpeedyImage;
			break;
		}
	}

	public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps) 
	{
		PhotonPlayer player = playerAndUpdatedProps[0] as PhotonPlayer;
		ExitGames.Client.Photon.Hashtable props = playerAndUpdatedProps [1] as ExitGames.Client.Photon.Hashtable;

		if (props.ContainsKey ("class") && player == PhotonPlayer) {
			PlayerConfiguration.PlayerClass currentClass = (PlayerConfiguration.PlayerClass)props ["class"];
			UpdatePlayerClassImage (currentClass);
		}
	}
}
