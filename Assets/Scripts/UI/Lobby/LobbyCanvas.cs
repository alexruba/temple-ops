﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LobbyCanvas : MonoBehaviour 
{
	[SerializeField]
	private RoomLayoutGroup _roomLayoutGroup;
	private RoomLayoutGroup RoomLayoutGroup
	{
		get { return _roomLayoutGroup; }
	}

	[SerializeField]
	private InputField _playerName;
	private InputField PlayerName
	{
		get { return _playerName; }
	}

	void Start()
	{
		PlayerName.text = PhotonNetwork.playerName;
		PlayerName.onValueChanged.AddListener (delegate {
			UpdatePlayerName ();
		});
	}

	private void UpdatePlayerName()
	{
		PhotonNetwork.playerName = PlayerName.text;
	}

	public void OnClickJoinRoom(string roomName)
	{
		if (PhotonNetwork.JoinRoom (roomName)) {
			
		} else {
			print ("Join room failed");
		}
	}
}
