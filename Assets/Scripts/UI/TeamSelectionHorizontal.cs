﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TeamSelectionHorizontal : MonoBehaviour
{
	public UserControl userControl;
	public List<Image> classPanels;
	public Color selectedColor;
	public Color notSelectedColor;

	[HideInInspector]
	public PlayerConfiguration.PlayerClass currentClass = PlayerConfiguration.PlayerClass.Explorer;

	private int currentSelectedClass = 0;
	private bool m_isAxisInUse = false;

	void Update()
	{
		if (Input.GetAxis (userControl.controllerDevice + "Horizontal") > 0.0f) {
			if (m_isAxisInUse == false) {
				classPanels [currentSelectedClass].color = notSelectedColor;

				if (currentSelectedClass == classPanels.Count - 1) {
					currentSelectedClass = 0;
				} else {
					currentSelectedClass++;
				}

				classPanels [currentSelectedClass].color = selectedColor;
				currentClass = (PlayerConfiguration.PlayerClass)currentSelectedClass;

				m_isAxisInUse = true;
			}

		} else if (Input.GetAxis (userControl.controllerDevice + "Horizontal") < 0f) {

			if (m_isAxisInUse == false) {
				classPanels [currentSelectedClass].color = notSelectedColor;

				if (currentSelectedClass == 0) {
					currentSelectedClass = classPanels.Count - 1;
				} else {
					currentSelectedClass--;
				}

				classPanels [currentSelectedClass].color = selectedColor;
				currentClass = (PlayerConfiguration.PlayerClass)currentSelectedClass;

				m_isAxisInUse = true;
			}

		}

		if(Input.GetAxis (userControl.controllerDevice + "Horizontal") == 0)
		{
			m_isAxisInUse = false;
		}    

	}
}
