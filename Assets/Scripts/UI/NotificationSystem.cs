﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NotificationSystem : MonoBehaviour {

    public List<GameObject> panels;

    void Start()
    {
        for (int i = 0; i < panels.Count; ++i)
            panels[i].gameObject.SetActive(false);
    }

    public void ChangePanelText(int player, string text)
    {
        panels[player].GetComponentInChildren<Text>().text = text;
    }

    public void OpenAllNotificationPanelsWithText(string text)
    {
        for (int i = 0; i < panels.Count; ++i)
            OpenNotificationPanelWithText(i, text);
    }

    public void CloseAllNotificationPanelsWithText()
    {
        for (int i = 0; i < panels.Count; ++i)
            CloseNotificationPanel(i);
    }

    public void OpenNotificationPanel(int player)
    {
        panels[player].gameObject.SetActive(true);
    }

    public void CloseNotificationPanel(int player)
    {
        panels[player].gameObject.SetActive(false);
    }

    public void OpenNotificationPanelWithText(int player, string text)
    {
        panels[player].GetComponentInChildren<Text>().text = text;
        StartCoroutine(OpenPanelDuring(player, 4.0f));
    }

    IEnumerator OpenPanelDuring(int player, float timeShown)
    {
        OpenNotificationPanel(player);
        yield return new WaitForSeconds(timeShown);
        CloseNotificationPanel(player);
    }


}
