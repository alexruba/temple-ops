﻿using UnityEngine;
using System.Collections;

public class CreatePlayerNetwork : MonoBehaviour {

	public GameObject playerNetworkObj;

	void Start()
	{
		GameObject playerNetwork = GameObject.Instantiate (playerNetworkObj);
		playerNetwork.name = "PlayerNetwork";
		playerNetwork.GetComponent<PhotonView> ().viewID = 999;
		playerNetwork.transform.SetParent (Transform.FindObjectOfType<DontDestroy>().transform);
	}
}
