﻿using UnityEngine;
using System.Collections;

public class TONetworkMovableObject : Photon.MonoBehaviour {

	Vector3 realPosition = Vector3.zero;
	Quaternion realRotation = Quaternion.identity;

	void Awake()
	{
		PhotonNetwork.sendRate = 60;
		PhotonNetwork.sendRateOnSerialize = 30;
	}

	void Update () 
	{
		if (photonView.isMine) {

		} else {
			transform.position = Vector3.Lerp (transform.position, realPosition, 0.25f);
			transform.rotation = Quaternion.RotateTowards (transform.rotation, realRotation, 500 * Time.deltaTime);
		}
	}

	public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		if (stream.isWriting) {
			stream.SendNext (transform.position);
			stream.SendNext (transform.rotation);
		} else {
			realPosition = (Vector3)stream.ReceiveNext ();
			realRotation = (Quaternion)stream.ReceiveNext ();
		}
	}
}
