﻿using UnityEngine;
using System.Collections;

public class PlayerNetwork : MonoBehaviour {

	public static PlayerNetwork Instance;
	public string PlayerName { get; private set; }
	private PhotonView PhotonView;
	private int PlayersInGame = 0;

	private void Awake()
	{
		Instance = this;
		PhotonView = GetComponent<PhotonView> ();
		PlayerName = "Default#" + Random.Range (1000, 9999);
	}

	[PunRPC]
	private void RPC_LoadGameOthers()
	{
		PhotonNetwork.LoadLevel (8);
	}

    [PunRPC]
    private void RPC_LeaveGame()
    {
        Cursor.visible = true;
        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel(1);
    }

	public void SpawnMyPlayer(Vector3 pos) 
	{
		if (PhotonNetwork.isMasterClient) {
			PhotonView.RPC ("RPC_LoadGameOthers", PhotonTargets.Others);
		}

		string team = PhotonNetwork.player.GetTeam () == PunTeams.Team.blue ? "B" : "R";
		string playerClass = PhotonNetwork.player.CustomProperties ["class"].ToString ();
		string playerToSpawn = playerClass + team + "_Network";

		GameObject myPlayer = (GameObject)PhotonNetwork.Instantiate (playerToSpawn, pos, Quaternion.identity, 0);
		myPlayer.GetComponentInChildren<CharControl> ().enabled = true;
		myPlayer.GetComponentInChildren<ShootingScript> ().enabled = true;
		myPlayer.GetComponentInChildren<ThrowObject> ().enabled = true;
        myPlayer.GetComponentInChildren<Minimap>().enabled = true;
        myPlayer.GetComponentInChildren<Character> ().enabled = true;
		myPlayer.GetComponentInChildren<PlayerStatus> ().enabled = true;
		myPlayer.GetComponentInChildren<MouseAimCamera> ().enabled = true;
		myPlayer.GetComponentInChildren<Camera> ().enabled = true;
		myPlayer.GetComponentInChildren<InitPlayerNetwork> ().enabled = true;
	}

	private void OnEnable()
	{
		MapGenerator.OnSpawnFound += SpawnMyPlayer;
	}

	private void OnDisable()
	{
		MapGenerator.OnSpawnFound -= SpawnMyPlayer;
	}
}
