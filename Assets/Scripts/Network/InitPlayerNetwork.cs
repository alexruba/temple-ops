﻿using UnityEngine;
using System.Collections;

public class InitPlayerNetwork : MonoBehaviour {

	void Start()
	{
		PlayerConfiguration playerConfig = GetComponentInChildren<PlayerConfiguration> ();
		playerConfig.SetUserControl (PlayerConfiguration.PlayerNumber.Single);

        Global.numberLocalPlayers = 1;
	}

}
