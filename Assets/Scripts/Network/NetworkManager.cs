﻿using UnityEngine;
using System.Collections;

public class NetworkManager : Photon.MonoBehaviour {

	//SpawnSpot[] spawnSpots;

	/// <summary>
	/// The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created.
	/// </summary>   
	[Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
	public byte MaxPlayersPerRoom = 8;

	void Start() {
		Connect ();
	}

	void Connect() {
		print ("Connecting to server...");

		if (!PhotonNetwork.connected) {
			PhotonNetwork.ConnectUsingSettings ("0.0.1");
		}
	}

	private void OnConnectedToMaster()
	{
		print ("Connected to master.");
		PhotonNetwork.playerName = PlayerNetwork.Instance.PlayerName;
		PhotonNetwork.JoinLobby (TypedLobby.Default);
	}

	void OnGUI() 
	{
		GUILayout.Label (PhotonNetwork.connectionStateDetailed.ToString ());
	}

	void OnJoinedLobby() 
	{
		print ("Joined lobby");
	}

	void OnPhotonRandomJoinFailed() 
	{
		PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = MaxPlayersPerRoom }, null);
	}

	void OnJoinedRoom() 
	{
		//SpawnMyPlayer ();
	}

	/*void SpawnMyPlayer() {
		if (spawnSpots == null) {
			Debug.LogError ("No spawn spots found.");
			return;
		}
		SpawnSpot mySpawnSpot = spawnSpots [Random.Range (0, spawnSpots.Length)];
		GameObject myPlayer = (GameObject)PhotonNetwork.Instantiate ("SoldierB_Network", mySpawnSpot.transform.position, mySpawnSpot.transform.rotation, 0);
		myPlayer.GetComponentInChildren<CharControl> ().enabled = true;
		myPlayer.GetComponentInChildren<ShootingScript> ().enabled = true;
		myPlayer.GetComponentInChildren<ThrowObject> ().enabled = true;
		myPlayer.GetComponentInChildren<Character> ().enabled = true;
		myPlayer.GetComponentInChildren<PlayerStatus> ().enabled = true;
		myPlayer.GetComponentInChildren<MouseAimCamera> ().enabled = true;
		myPlayer.GetComponentInChildren<Camera> ().enabled = true;
	}*/
}
