﻿using UnityEngine;
using System.Collections;

public class LobbyNetwork : MonoBehaviour {

	public GameObject connectingPanel;

	void Awake()
	{
		connectingPanel.SetActive (true);
	}

	void Start() 
	{
		Connect ();
	}

	void Connect() 
	{
		print ("Connecting to server...");
		PhotonNetwork.ConnectUsingSettings ("0.0.1");
	}

	private void OnConnectedToMaster()
	{
		print ("Connected to master.");
		PhotonNetwork.automaticallySyncScene = false;
		PhotonNetwork.playerName = PlayerNetwork.Instance.PlayerName;
		PhotonNetwork.JoinLobby (TypedLobby.Default);
	}

	void OnJoinedLobby() 
	{
		connectingPanel.SetActive (false);

		if (!PhotonNetwork.inRoom) {
			MainCanvasManager.Instance.LobbyCanvas.transform.SetAsLastSibling ();
		}

		PhotonNetwork.playerName = PlayerNetwork.Instance.PlayerName;
		print ("Joined lobby");
	}
}
