﻿using UnityEngine;
using System.Collections;

public class GameplayNetwork : MonoBehaviour {

	public void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps) 
	{
		PhotonPlayer player = playerAndUpdatedProps[0] as PhotonPlayer;
		ExitGames.Client.Photon.Hashtable props = playerAndUpdatedProps [1] as ExitGames.Client.Photon.Hashtable;

		if (props.ContainsKey ("shield")) {
			Transform playerTransform = PhotonView.Find ((int)props ["shield"]).transform;
			playerTransform.Find ("Shield").gameObject.SetActive (true);
		}

		if (props.ContainsKey ("unshield")) {
			Transform playerTransform = PhotonView.Find ((int)props ["unshield"]).transform;
			playerTransform.Find ("Shield").gameObject.SetActive (false);
		}

		if (props.ContainsKey ("disappear")) {
			Renderer[] renderers = PhotonView.Find ((int)props ["disappear"]).gameObject.GetComponentsInChildren<Renderer> ();

			foreach (Renderer renderer in renderers) {
				renderer.enabled = false;
			}
		}

		if (props.ContainsKey ("appear")) {
			Renderer[] renderers = PhotonView.Find ((int)props ["appear"]).gameObject.GetComponentsInChildren<Renderer> ();

			foreach (Renderer renderer in renderers) {
				renderer.enabled = true;
			}
		}
	}

	[PunRPC]
	void RPC_NotifyPlayersDisconnect(PhotonPlayer photonPlayer)
	{
		GameObject.Find("NotificationSystem").GetComponent<NotificationSystem>().OpenAllNotificationPanelsWithText("Player " + photonPlayer.NickName + " left");
	}

    [PunRPC]
    void RPC_PlayerDieAnimation(int viewID)
    {
        Transform player = PhotonView.Find(viewID).transform;
        player.GetComponentInChildren<Animator>().SetTrigger("Die");
        print("Die Animation");
    }

    [PunRPC]
    void RPC_PlayerResurrectAnimation(int viewID)
    {
        Transform player = PhotonView.Find(viewID).transform;
        player.GetComponentInChildren<Animator>().SetTrigger("Resurrect");
        print("Resurrect Animation");
    }

    private void OnPhotonPlayerDisconnected(PhotonPlayer photonPlayer)
	{
		GetComponent<PhotonView> ().RPC ("RPC_NotifyPlayersDisconnect", PhotonTargets.All, photonPlayer);
		PhotonNetwork.DestroyPlayerObjects (photonPlayer);
	}
}
