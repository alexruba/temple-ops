﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Minimap : MonoBehaviour {
	
	private Image minimapImage;
	private GameObject container;
	public int rows;
	public int cols;
	public Texture2D areaTex;
	private PlayerConfiguration playerConfig;
	private Color playerColor;
	private Texture2D minimapTex;
	private Vector3 lastPosition;
	private Vector3 explorerLastPosition;
	private PlayerConfiguration.PlayerTeam team;
	private List<TeamMember> teamMembers;
	private List<Vector3> lastTeamMembersPos;
	
	Color pathColor = Color.black;
	Color explorerColor = Color.yellow;
	Color hardPointColor = Color.magenta;
	Color extractionPointColor = Color.cyan;
	Color defaultColor = Color.clear;
	//Color wallsColor = new Color((float)174.0/255, (float)143.0/255 , (float)101.0/255, 1);
	
	void Start()
	{
        playerConfig = GetComponent<PlayerConfiguration>();
		team = playerConfig.team;
		playerColor = team == PlayerConfiguration.PlayerTeam.BlueTeam ? Color.blue : Color.red;
		lastPosition = gameObject.transform.position;
		explorerLastPosition = team == PlayerConfiguration.PlayerTeam.BlueTeam ? Global.blueExplorerPos : Global.redExplorerPos;

        if(PhotonNetwork.offlineMode) {
            SetupLocalPlayers();
        } else {
            SetupOnlinePlayers();
        }

		lastTeamMembersPos = new List<Vector3>();
		for (int i = 0; i < teamMembers.Count; ++i)
			lastTeamMembersPos.Add(teamMembers[i].memberObj.transform.position);
        
        SetupUI();
        SetupMinimap();
	}

    private void SetupLocalPlayers()
    {
        teamMembers = team == PlayerConfiguration.PlayerTeam.BlueTeam ?
            TeamManager.BlueTeam : TeamManager.RedTeam;
    }

    private void SetupOnlinePlayers()
    {
        teamMembers = new List<TeamMember>();

        PlayerConfiguration[] characters = FindObjectsOfType<PlayerConfiguration>();
        foreach(PlayerConfiguration character in characters)
        {
            TeamMember tm = new TeamMember();
            tm.c = character.playerClass;
            tm.team = character.team;
            tm.memberObj = character.gameObject;

            teamMembers.Add(tm);
        }
    }

    private void SetupUI()
    {
        container = GameObject.Find("MapPanel" + (int)playerConfig.playerNumber);
        minimapImage = GameObject.Find("MinimapImage" + (int)playerConfig.playerNumber).GetComponent<Image>();
        container.SetActive(false);
    }

    private void SetupMinimap()
    {
        minimapTex = new Texture2D(rows, cols);
        minimapImage.GetComponent<Image>().material.mainTexture = minimapTex;

        for (int x = 0; x < rows; ++x)
        {
            for (int y = 0; y < cols; ++y)
            {
                minimapTex.SetPixel(x, y, defaultColor);
            }
        }

        minimapTex.Apply();
    }
	
	void Update()
	{
		if (playerConfig.userControl.isCrouching())
			container.SetActive(true);
		else
			container.SetActive(false);
		
		if(minimapImage.gameObject.activeInHierarchy)
		{
			minimapImage.GetComponent<Image>().material.mainTexture = minimapTex;
		}
		
		for (int i = 0; i < lastTeamMembersPos.Count; ++i)
		{
			drawCircle(lastTeamMembersPos[i], 3, defaultColor, false);
		}
		
		drawCircle(lastPosition, 10, pathColor, true);
		drawCircle(explorerLastPosition, 10, pathColor, true);

		for (int i = 0; i < lastTeamMembersPos.Count; ++i)
		{
			drawInterestPoint(lastTeamMembersPos[i], playerColor);
		}
		
		float ang = gameObject.transform.eulerAngles.y;
		
		if (team == PlayerConfiguration.PlayerTeam.BlueTeam)
		{
			if(Global.blueFoundHardPoint)
			{
				drawInterestPoint(Global.hardPointPos, hardPointColor);
			}
			if(Global.BlueExtractionPointDiscovered)
			{
				drawInterestPoint(Global.BlueExtractionPointPos, extractionPointColor);
			}

			drawPlayerPosArrow(gameObject.transform.position, gameObject.transform.eulerAngles.y, playerColor);
			drawPlayerPosArrow(Global.blueExplorerPos, ang, explorerColor);
			explorerLastPosition = Global.blueExplorerPos;
		}
		
		else if(team == PlayerConfiguration.PlayerTeam.RedTeam)
		{
			if(Global.redFoundHardPoint)
			{
				drawInterestPoint(Global.hardPointPos, hardPointColor);
			}
			if(Global.RedExtractionPointDiscovered)
			{
				drawInterestPoint(Global.RedExtractionPointPos, extractionPointColor);
			}

			drawPlayerPosArrow(gameObject.transform.position, gameObject.transform.eulerAngles.y, playerColor);
			drawPlayerPosArrow(Global.redExplorerPos, ang, explorerColor);
			explorerLastPosition = Global.redExplorerPos;
		}
		
		lastPosition = gameObject.transform.position;
		
		for (int i = 0; i < teamMembers.Count; ++i)
			lastTeamMembersPos[i] = teamMembers[i].memberObj.transform.position;
		
		minimapTex.Apply();
	}
	
	void drawCircle(Vector3 pos, int r, Color col, bool mapMask)
	{
		//bool pixelDrawn = false;
		
		minimapTex.SetPixel((int)pos.x, (int)pos.z, col);
		
		
		for(int k = 0; k < 2; k++)
		{
			for(int i = (int)pos.x - r; i < (int)pos.x + r; ++i)
			{
				for(int j = (int)pos.z - r; j < (int)pos.z + r; ++j)
				{
					checkCircleAndDrawPixel(pos, r, i, j, col, mapMask);
				}
			}
		}
		
	}
	
	bool checkCircleAndDrawPixel(Vector3 pos, int r, int i, int j, Color col, bool mapMask)
	{
		bool drawn = false;
		
		if(mapMask)
		{
			if(isVisible(i, j))
			{
				if(distSquare(i, j, (int)pos.x, (int)pos.z) < r*r)
				{
					if(Global.mapTexture.GetPixel(i, j).a != 0)
					{
						minimapTex.SetPixel(i, j, col);
						drawn = true;
					}
					else
					{
						minimapTex.SetPixel(i, j, defaultColor);
					}
				}
			}
		}
		else
		{
			minimapTex.SetPixel(i, j, col);
		}
		
		return drawn;
	}
	
	void paintMinimap(int x, int y, Color col)
	{
		if(Global.mapTexture.GetPixel(x, y).a != 0)
		{
			minimapTex.SetPixel(x, y, col);
		}
	}
	
	bool isVisible(int x, int y)
	{
		bool visible = false;
		
		if(minimapTex.GetPixel(x, y).a != 0)
		{
			visible = true;
		}
		else
		{
			for(int i = x-1; i <= x+1 && !visible; ++i)
			{
				for(int j = y-1; j <= y+1 && !visible; ++j)
				{
					if(minimapTex.GetPixel(i, j).a != 0)
					{
						visible = true;
					}
				}
			}
		}
		
		return visible;
	}
	
	void drawInterestPoint(Vector3 pos, Color col)
	{
		minimapTex.SetPixel((int)pos.x - 2, (int)pos.z + 1, col);
		minimapTex.SetPixel((int)pos.x - 2, (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x - 2, (int)pos.z - 1, col);
		
		minimapTex.SetPixel((int)pos.x + 2, (int)pos.z + 1, col);
		minimapTex.SetPixel((int)pos.x + 2, (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x + 2, (int)pos.z - 1, col);
		
		minimapTex.SetPixel((int)pos.x + 1, (int)pos.z + 2, col);
		minimapTex.SetPixel((int)pos.x,     (int)pos.z + 2, col);
		minimapTex.SetPixel((int)pos.x - 1, (int)pos.z + 2, col);
		
		minimapTex.SetPixel((int)pos.x + 1, (int)pos.z - 2, col);
		minimapTex.SetPixel((int)pos.x,     (int)pos.z - 2, col);
		minimapTex.SetPixel((int)pos.x - 1, (int)pos.z - 2, col);
	}
	
	void drawPlayerPosCircle(Vector3 pos, Color col)
	{
		// Circle
		minimapTex.SetPixel((int)pos.x,     (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x + 1, (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x - 1, (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x,     (int)pos.z + 1, col);
		minimapTex.SetPixel((int)pos.x,     (int)pos.z - 1, col);
		
		minimapTex.SetPixel((int)pos.x+1,   (int)pos.z+1, col);
		minimapTex.SetPixel((int)pos.x-1,   (int)pos.z+1, col);
		minimapTex.SetPixel((int)pos.x+1,   (int)pos.z-1, col);
		minimapTex.SetPixel((int)pos.x-1,   (int)pos.z-1, col);
		
		minimapTex.SetPixel((int)pos.x-2,   (int)pos.z+1, col);
		minimapTex.SetPixel((int)pos.x-2,   (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x-2,   (int)pos.z-1, col);
		
		minimapTex.SetPixel((int)pos.x+2,   (int)pos.z+1, col);
		minimapTex.SetPixel((int)pos.x+2,   (int)pos.z, col);
		minimapTex.SetPixel((int)pos.x+2,   (int)pos.z-1, col);
		
		minimapTex.SetPixel((int)pos.x+1,   (int)pos.z+2, col);
		minimapTex.SetPixel((int)pos.x,     (int)pos.z+2, col);
		minimapTex.SetPixel((int)pos.x-1,   (int)pos.z+2, col);
		
		minimapTex.SetPixel((int)pos.x+1,   (int)pos.z-2, col);
		minimapTex.SetPixel((int)pos.x,     (int)pos.z-2, col);
		minimapTex.SetPixel((int)pos.x-1,   (int)pos.z-2, col);
	}
	
	void drawPlayerPosArrow(Vector3 pos, float ang, Color col)
	{
		// Arrow
		//Debug.Log(ang);
		
		// Sentido: 0 Derecha, 1 Izquierda, 2 Arriba, 3 Abajo
		if     (ang >= 360.0f - 22.5f || ang < 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 2, col);
		}
		
		else if(ang >= 90.0f - 22.5f && ang < 90.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 0, col);
		}
		
		else if(ang >= 180.0f - 22.5f && ang < 180.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 3, col);
		}
		
		else if(ang >= 270.0f - 22.5f && ang < 270.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 1, col);
		}
		
		// Diagonales
		else if(ang >= 45.0f - 22.5f && ang < 45.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 5, col);
		}
		
		else if(ang >= 135.0f - 22.5f && ang < 135.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 7, col);
		}
		
		else if(ang >= 225.0f - 22.5f && ang < 225.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 4, col);
		}
		else if(ang >= 315.0f - 22.5f && ang < 315.0f + 22.5f)
		{
			drawArrow((int)pos.x, (int)pos.z, 6, col);
		}
	}
	
	void drawArrow(int x, int y, int sentido, Color col)
	{
		int p1 = 1, p2 = 2, p3 = 3;
		if(sentido == 1 || sentido == 3 || sentido == 4 || sentido == 6)
		{
			p1 = -1; p2 = -2; p3 = -3;
		}
		
		if(sentido <= 1)
		{
			minimapTex.SetPixel(x + p2, y,      col);
			
			minimapTex.SetPixel(x + p1, y + p1, col);
			minimapTex.SetPixel(x + p1, y     , col);
			minimapTex.SetPixel(x + p1, y - p1, col);
			
			minimapTex.SetPixel(x,      y + p1, col);
			minimapTex.SetPixel(x,      y     , col);
			minimapTex.SetPixel(x,      y - p1, col);
			
			minimapTex.SetPixel(x - p1, y + p2, col);
			minimapTex.SetPixel(x - p1, y + p1, col);
			minimapTex.SetPixel(x - p1, y - p1, col);
			minimapTex.SetPixel(x - p1, y - p2, col);
			
			minimapTex.SetPixel(x - p2, y + p2, col);
			minimapTex.SetPixel(x - p2, y - p2, col);
		}
		else if(sentido <= 3)
		{
			minimapTex.SetPixel(x, y + p2,      col);
			
			minimapTex.SetPixel(x + p1, y + p1, col);
			minimapTex.SetPixel(x     , y + p1, col);
			minimapTex.SetPixel(x + p1, y + p1, col);
			
			minimapTex.SetPixel(x + p1,      y, col);
			minimapTex.SetPixel(x     ,      y, col);
			minimapTex.SetPixel(x - p1,      y, col);
			
			minimapTex.SetPixel(x + p2, y - p1, col);
			minimapTex.SetPixel(x + p1, y - p1, col);
			minimapTex.SetPixel(x - p1, y - p1, col);
			minimapTex.SetPixel(x - p2, y - p1, col);
			
			minimapTex.SetPixel(x + p2, y - p2, col);
			minimapTex.SetPixel(x - p2, y - p2, col);
		}
		else if(sentido <= 5)
		{
			minimapTex.SetPixel(x + p2, y + p2, col);
			minimapTex.SetPixel(x + p1, y + p2, col);
			
			minimapTex.SetPixel(x + p2, y + p1, col);
			minimapTex.SetPixel(x + p1, y + p1, col);
			minimapTex.SetPixel(x     , y + p1, col);
			minimapTex.SetPixel(x - p1, y + p1, col);
			
			minimapTex.SetPixel(x + p1, y     , col);
			minimapTex.SetPixel(x,      y     , col);
			minimapTex.SetPixel(x - p1, y     , col);
			minimapTex.SetPixel(x - p2, y     , col);
			minimapTex.SetPixel(x - p3, y     , col);
			
			minimapTex.SetPixel(x + p1, y - p1, col);
			minimapTex.SetPixel(x     , y - p1, col);
			
			minimapTex.SetPixel(x     , y - p2, col);
			minimapTex.SetPixel(x     , y - p3, col);
		}
		else
		{
			minimapTex.SetPixel(x + p2, y - p2, col);
			minimapTex.SetPixel(x + p2, y - p1, col);
			
			minimapTex.SetPixel(x + p1, y - p1, col);
			minimapTex.SetPixel(x + p1, y - p1, col);
			minimapTex.SetPixel(x + p1, y     , col);
			minimapTex.SetPixel(x + p1, y + p1, col);
			
			minimapTex.SetPixel(x     , y - p1, col);
			minimapTex.SetPixel(x     , y     , col);
			minimapTex.SetPixel(x     , y + p1, col);
			minimapTex.SetPixel(x     , y + p2, col);
			minimapTex.SetPixel(x     , y + p3, col);
			
			minimapTex.SetPixel(x - p1, y - p1, col);
			minimapTex.SetPixel(x - p1, y     , col);
			
			minimapTex.SetPixel(x - p2, y     , col);
			minimapTex.SetPixel(x - p3, y     , col);
		}
	}
	
	int distSquare(int X1, int Y1, int X2, int Y2)
	{
		int distX = Mathf.Abs(X1-X2);
		int distY = Mathf.Abs(Y1-Y2);
		return distX * distX + distY * distY;
	}
	
	public Texture2D getMinimapTex()
	{
		return minimapTex;
	}
}
