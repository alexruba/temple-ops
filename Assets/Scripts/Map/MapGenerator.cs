﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MapGenerator : MonoBehaviour
{
	public GameObject HardPoint;
	public GameObject MapWallColumn;
	public GameObject MapWallColumnHalf;
	public GameObject MapWall;
	public GameObject MapWallD;
	public GameObject MapWall_B1;
	public GameObject MapWallD_B1;
	public GameObject MapWall_B2;
	public GameObject MapWallD_B2;
	public GameObject MapWall_B3;
	public GameObject MapWallD_B3;
	public GameObject MapWall_S;
	public GameObject MapWallD_S;
	public GameObject MapWall_URJC;
	public GameObject MapWallD_URJC;
	public GameObject MapWall_Torch;
	public GameObject MapWallD_Torch;
	public GameObject MapFloor;
	public GameObject MapRoof;
	
	public int rows;
	public int cols;
	public int MID_RADIUS;
	public int MIDWALL_SIZE;
	public float WALL_PCT;
	public float MIDWALL_PCT;

	public Transform BlueTeamExit;
	public Transform RedTeamExit;
	
	private const int Wall = 1;
	private const int Floor = 0;
	private const int Spawn = 2;
	private const int Exit = 3;
	
	private int[,] map;
	private int[,] map2;
	private int[,] mapAux;
	private int[,] torchs;
	private Vector3 midPoint;

	public delegate void MapGeneration(Vector3 pos);
	public static event MapGeneration OnSpawnFound; 
	
    // Use this for initialization
	void Awake()
	{
		map = new int[rows, cols];
		map2 = new int[rows, cols];
		mapAux = new int[rows, cols];
		torchs = new int[rows, cols];
		midPoint = new Vector3(rows / 2.0f, 0.0f, cols / 2.0f);
		int nWalkable1, nWalkable2;
		
        // Suelo
		GameObject plane = GameObject.Instantiate(MapFloor, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity) as GameObject;
		plane.transform.localScale = new Vector3(rows / 8.5f, 1.0f, cols / 8.5f);
		plane.transform.position = new Vector3(rows / 2.0f - 0.5f, 0.0f, cols / 2.0f - 0.5f);
		
		GameObject roof = GameObject.Instantiate(MapRoof, new Vector3(0.0f, 0.0f, 0.0f), Quaternion.identity) as GameObject;
		roof.transform.localScale = new Vector3(rows, 1.0f, cols);
		roof.transform.position = new Vector3(rows / 2.0f - 0.5f, 7.5f, cols / 2.0f - 0.5f); //7.5
		
		do
		{
			InitMap();
			GenerateMap();
			
			nWalkable1 = EnsureConnectivity(map, mapAux);
			nWalkable2 = EnsureConnectivity(map2, mapAux);
			
			if (nWalkable1 < 0.2 * rows * cols || nWalkable1 > 0.65 * rows * cols)
			{
				map = map2;
				nWalkable1 = nWalkable2;
				//Debug.Log("Map Change");
			}
			
			PlaceSpawnsAndExits(map);
			MakeTopPartRadialSym(map);
			//PlaceSpawnsAndExits(map2);
		} while (nWalkable1 < 0.50 * rows * cols);
		
		CreateMap();
		
        // Punto caliente
		HardPoint.transform.position = midPoint;
		Global.mapCols = cols;
		Global.mapRows = rows;
		Global.hardPointPos = midPoint;
		Global.BlueExtractionPointPos = BlueTeamExit.position;
		Global.RedExtractionPointPos = RedTeamExit.position;
		
		MapTex();
		Global.mapReady = true;
	}
	
	void MapTex()
	{
		Global.mapTexture = new Texture2D(rows, cols);
		
		for(int x = 0; x < rows; ++x)
		{
			for(int y = 0; y < cols; ++y)
			{
				if(map[x, y] != Wall)
					Global.mapTexture.SetPixel(x, y, Color.black);
				else
					Global.mapTexture.SetPixel(x, y, Color.clear);
			}
		}
		
		Global.mapTexture.Apply();
		
		Global.mapFogTexture = new Texture2D(rows, cols);
		
		for(int x = 0; x < rows; ++x)
		{
			for(int y = 0; y < cols; ++y)
			{
				if(map[x, y] != Wall)
					Global.mapFogTexture.SetPixel(x, y, Color.white);
				else
					Global.mapFogTexture.SetPixel(x, y, Color.clear);
			}
		}
		
		Global.mapFogTexture.Apply();
	}
	
	bool IsBorderWall(int x, int y)
	{
		bool isBorder = false;
		
		if(map[x,y] == Wall)
		{
			if (x > 0 && map[x - 1, y] != Wall) isBorder = true;
			else if (x < rows - 1 && map[x + 1, y] != Wall) isBorder = true;
			else if (y > 0 && map[x, y - 1] != Wall) isBorder = true;
			else if (y < cols - 1 && map[x, y + 1] != Wall) isBorder = true;
		}
		
		return isBorder;
	}
	
	bool IsBorderFloor(int x, int y)
	{
		bool isBorder = false;
		
		if(map[x,y] != Wall)
		{
			int initX = -1, initY = -1, endX = 1, endY = 1;
			
			if(x == 0) initX = 0;
			else if(x == rows-1) endX = 0;
			if(y == 0) initY = 0;
			else if(y == cols-1) endY = 0;
			
			for(int i = initX; i < endX; ++i)
			{
				for(int j = initY; j < endY; ++j)
				{
					if(map[x+i,y+j] == Wall) isBorder = true;
				}
			}
		}
		
		return isBorder;
	}
	
	bool TorchsArround(int x, int y, int radius)
	{
		bool isTorch = false;
		
		for(int i = x-radius; i <= x+radius && !isTorch; ++i)
		{
			for(int j = y-radius; j <= y+radius && !isTorch; ++j)
			{
				if(i >= 0 && i < rows && j >= 0 && j < cols)
				{
					if(torchs[i,j] == 1)
					{
						isTorch = true;
					}
				}
			}
		}
		
		if(!isTorch) torchs[x,y] = 1;
		
		return  isTorch;
	}
	
	int IsSaliente(int x, int y)
	{
		// · · #    # # #    · · ·   # · ·
		// · # #    · # ·    · # ·   # # ·
		// · · #    · · ·    # # #   # · ·
		int isSaliente = 0;
		
		if (map[(x - 1), (y - 1)] != Wall &&
			map[(x - 1), (y - 0)] != Wall &&
			//map[(x - 1), (y + 1)] == Wall &&
			map[(x + 0), (y - 1)] != Wall &&
			map[(x + 0), (y + 1)] == Wall &&
			map[(x + 1), (y - 1)] != Wall &&
			map[(x + 1), (y - 0)] != Wall)// &&
			//map[(x + 1), (y + 1)] == Wall)
		{
			isSaliente = 1;
		}
		else if (//map[(x - 1), (y - 1)] == Wall &&
			     map[(x - 1), (y - 0)] == Wall &&
			     //map[(x - 1), (y + 1)] == Wall &&
			     map[(x + 0), (y - 1)] != Wall &&
			     map[(x + 0), (y + 1)] != Wall &&
			     map[(x + 1), (y - 1)] != Wall &&
			     map[(x + 1), (y - 0)] != Wall &&
			     map[(x + 1), (y + 1)] != Wall)
		{
			isSaliente = 2;
		}
		else if (map[(x - 1), (y - 1)] != Wall &&
			     map[(x - 1), (y - 0)] != Wall &&
			     map[(x - 1), (y + 1)] != Wall &&
			     map[(x + 0), (y - 1)] != Wall &&
			     map[(x + 0), (y + 1)] != Wall &&
			     //map[(x + 1), (y - 1)] == Wall &&
			     map[(x + 1), (y - 0)] == Wall)// &&
			     //map[(x + 1), (y + 1)] == Wall)
		{
			isSaliente = 3;
		}
		else if (//map[(x - 1), (y - 1)] == Wall &&
			     map[(x - 1), (y - 0)] != Wall &&
			     map[(x - 1), (y + 1)] != Wall &&
			     map[(x + 0), (y - 1)] == Wall &&
			     map[(x + 0), (y + 1)] != Wall &&
			     //map[(x + 1), (y - 1)] == Wall &&
			     map[(x + 1), (y - 0)] != Wall &&
			     map[(x + 1), (y + 1)] != Wall)
		{
			isSaliente = 4;
		}
		
		return isSaliente;
	}
	
	bool IsIsla(int x, int y)
	{
		// · · ·    · # · 
		// · # #    # # # 
		// · # #    · # · 
		bool isIsla = false;
		
		if (map[(x - 1), (y - 1)] != Wall &&
			map[(x - 1), (y - 0)] != Wall &&
			map[(x - 1), (y + 1)] != Wall &&
			map[(x + 0), (y - 1)] != Wall &&
			//map[(x + 0), (y + 1)] == Wall &&
			map[(x + 1), (y - 1)] != Wall &&
			//map[(x + 1), (y - 0)] == Wall &&
			map[(x + 1), (y + 1)] == Wall &&
			//map[(x - 1), (y + 2)] != Wall &&
			map[(x + 0), (y + 2)] != Wall &&
			map[(x + 1), (y + 2)] != Wall &&
			map[(x + 2), (y - 1)] != Wall &&
			map[(x + 2), (y + 0)] != Wall &&
			map[(x + 2), (y + 1)] != Wall &&
			map[(x + 2), (y + 2)] != Wall)
		{
			isIsla = true;
		}
		
		return isIsla;
	}
	
	bool PlaceColumn(int x, int y)
	{
		bool ColumnPlaced = false;
		GameObject m;
		float sal = 0;
		if(x > 0 && x < rows-1 && y > 0 && y < cols-1 && (WallsArround(map, x, y) == 0 || (sal = IsSaliente(x, y)) != 0))
		{
			if(sal == 1) m = GameObject.Instantiate(MapWallColumn, new Vector3((float)x     , 0.0f, (float)y+0.6f), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
			else if(sal == 2) m = GameObject.Instantiate(MapWallColumn, new Vector3((float)x-0.6f, 0.0f, (float)y     ), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
			else if(sal == 3) m = GameObject.Instantiate(MapWallColumn, new Vector3((float)x+0.6f, 0.0f, (float)y     ), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
			else if(sal == 4) m = GameObject.Instantiate(MapWallColumn, new Vector3((float)x     , 0.0f, (float)y-0.6f), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
			else m = GameObject.Instantiate(MapWallColumn, new Vector3((float)x, 0.0f, (float)y), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
			
			m.transform.parent = gameObject.transform;
			ColumnPlaced = true;
		}
		else if(x > 0 && x < rows-2 && y > 0 && y < cols-2 && IsIsla(x, y))
		{
			m = GameObject.Instantiate(MapWallColumn, new Vector3((float)x+0.48f, 0.0f, (float)y+0.48f), Quaternion.Euler(-90.0f, 0.0f, 0.0f)) as GameObject;
			m.transform.parent = gameObject.transform;
			m.transform.localScale = new Vector3(1.2f, 1.2f, 1.00f);
			ColumnPlaced = true;
		}
		return ColumnPlaced;
	}
	
	void PlaceWalls(int x, int y)
	{
		GameObject m;
		GameObject mw = MapWall;
		float h = 4.0f;
		//float torch_rate = 10.0f;
		bool isTorch = false;
		
		// Wall types
		int WallType = 0;
		float randWall = Random.Range(0.0f, 100.0f);
		if     (randWall < 8.0f) WallType = 1;
		else if(randWall < 16.0f) WallType = 2;
		else if(randWall < 24.0f) WallType = 3;
		else if(randWall < 32.0f) WallType = 4;
		else if(randWall < 40.0f) WallType = 5;
		
		if(!PlaceColumn(x, y))
		{
			isTorch = !TorchsArround(x, y, 7);
			if(isTorch == true) torchs[x,y] = 1;
		}
		
		if(x < rows-1 && IsBorderWall(x+1, y))
		{
			if(y > 0 && map[x+1, y-1] != Wall)
			{
				// Horizontal mirando hacia arriba
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWall;
				if(isTorch) mw = MapWall_Torch;
				else
				{
					if     (WallType == 0) mw = MapWall;
					else if(WallType == 1) mw = MapWall_B1;
					else if(WallType == 2) mw = MapWall_B2;
					else if(WallType == 3) mw = MapWall_B3;
					else if(WallType == 4) mw = MapWall_S;
					else if(WallType == 5) mw = MapWall_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x+0.5f, h, (float)y), Quaternion.Euler(90.0f, 180.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
			
			if(y < cols-1 && map[x+1, y+1] != Wall)
			{
				// Horizontal mirando hacia abajo
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWall;
				if(isTorch) mw = MapWall_Torch;
				else
				{
					if     (WallType == 0) mw = MapWall;
					else if(WallType == 1) mw = MapWall_B1;
					else if(WallType == 2) mw = MapWall_B2;
					else if(WallType == 3) mw = MapWall_B3;
					else if(WallType == 4) mw = MapWall_S;
					else if(WallType == 5) mw = MapWall_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x+0.5f, h, (float)y), Quaternion.Euler(90.0f, 0.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
		}
		
		if(x > 0 && y < cols-1 && IsBorderWall(x-1, y+1))
		{
			if(map[x-1, y] != Wall)
			{
				// Diagonal izquierda mirando hacia arriba
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWallD;
				if(isTorch) mw = MapWallD_Torch;
				else
				{
					if     (WallType == 0) mw = MapWallD;
					else if(WallType == 1) mw = MapWallD_B1;
					else if(WallType == 2) mw = MapWallD_B2;
					else if(WallType == 3) mw = MapWallD_B3;
					else if(WallType == 4) mw = MapWallD_S;
					else if(WallType == 5) mw = MapWallD_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x-0.5f, h, (float)y+0.5f), Quaternion.Euler(90.0f, Mathf.Rad2Deg * 5*Mathf.PI/4.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
			
			if(map[x, y+1] != Wall)
			{
				// Diagonal izquierda mirando hacia abajo
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWallD;
				if(isTorch) mw = MapWallD_Torch;
				else
				{
					if     (WallType == 0) mw = MapWallD;
					else if(WallType == 1) mw = MapWallD_B1;
					else if(WallType == 2) mw = MapWallD_B2;
					else if(WallType == 3) mw = MapWallD_B3;
					else if(WallType == 4) mw = MapWallD_S;
					else if(WallType == 5) mw = MapWallD_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x-0.5f, h, (float)y+0.5f), Quaternion.Euler(90.0f, 45.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
		}
		
		if(y < cols-1 && IsBorderWall(x, y+1))
		{
			if(x > 0 && map[x-1, y+1] != Wall)
			{
				// Vertical mirando hacia la izquierda
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWall;
				if(isTorch) mw = MapWall_Torch;
				else
				{
					if     (WallType == 0) mw = MapWall;
					else if(WallType == 1) mw = MapWall_B1;
					else if(WallType == 2) mw = MapWall_B2;
					else if(WallType == 3) mw = MapWall_B3;
					else if(WallType == 4) mw = MapWall_S;
					else if(WallType == 5) mw = MapWall_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x, h, (float)y+0.5f), Quaternion.Euler(90.0f, 270.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
			
			if(x < rows-1 && map[x+1, y+1] != Wall)
			{
				// Vertical mirando hacia la derecha
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWall;
				if(isTorch) mw = MapWall_Torch;
				else
				{
					if     (WallType == 0) mw = MapWall;
					else if(WallType == 1) mw = MapWall_B1;
					else if(WallType == 2) mw = MapWall_B2;
					else if(WallType == 3) mw = MapWall_B3;
					else if(WallType == 4) mw = MapWall_S;
					else if(WallType == 5) mw = MapWall_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x, h, (float)y+0.5f), Quaternion.Euler(90.0f, 90.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
		}
		
		if(x < rows-1 && y < cols-1 && IsBorderWall(x+1, y+1))
		{
			if(map[x+1, y] != Wall)
			{
				// Diagonal derecha mirando hacia arriba
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWallD;
				if(isTorch) mw = MapWallD_Torch;
				else
				{
					if     (WallType == 0) mw = MapWallD;
					else if(WallType == 1) mw = MapWallD_B1;
					else if(WallType == 2) mw = MapWallD_B2;
					else if(WallType == 3) mw = MapWallD_B3;
					else if(WallType == 4) mw = MapWallD_S;
					else if(WallType == 5) mw = MapWallD_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x+0.5f, h, (float)y+0.5f), Quaternion.Euler(90.0f, 3*45.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
			
			if(map[x, y+1] != Wall)
			{
				// Diagonal derecha mirando hacia abajo
				//if(Random.Range(0.0f, 100.0f) > torch_rate) mw = MapWallD;
				if(isTorch) mw = MapWallD_Torch;
				else
				{
					if     (WallType == 0) mw = MapWallD;
					else if(WallType == 1) mw = MapWallD_B1;
					else if(WallType == 2) mw = MapWallD_B2;
					else if(WallType == 3) mw = MapWallD_B3;
					else if(WallType == 4) mw = MapWallD_S;
					else if(WallType == 5) mw = MapWallD_URJC;
				}
				
				m = GameObject.Instantiate(mw, new Vector3((float)x+0.5f, h, (float)y+0.5f), Quaternion.Euler(90.0f, 7*45.0f, 0.0f)) as GameObject;
				m.transform.parent = gameObject.transform;
				isTorch = false;
			}
		}
	}
	
	void CreateMap()
	{
		if (!PhotonNetwork.offlineMode && !PhotonNetwork.isMasterClient) {
			map = DataSerialize.Deserialize<int[,]>((byte[])PhotonNetwork.room.CustomProperties ["map"]);
		}

		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				torchs[i,j] = 0;
			}
		}
		
		for (int i = 0; i < rows; i++)
		{
			for (int j = 0; j < cols; j++)
			{
				if (map[i, j] == Wall)
				{
					if (IsBorderWall(i, j))
					{
						PlaceWalls(i, j);
						//GameObject m = GameObject.Instantiate(MapWallColumn, new Vector3((float)i, 0.0f, (float)j), Quaternion.EulerAngles(0.0f, 0.0f, 0.0f)) as GameObject;
						//m.transform.parent = gameObject.transform;
					}
				}
				else if (map[i, j] == Spawn)
				{
					if (j < midPoint.z)
					{
						if (PhotonNetwork.offlineMode) {
							for (int k = 0; k < TeamManager.BlueTeam.Count; k++) {
								TeamManager.Spawn (TeamManager.BlueTeam [k], new Vector3 ((float)i, 0.0f, (float)j));
							}
						} else {
							if (OnSpawnFound != null && PhotonNetwork.player.GetTeam () == PunTeams.Team.blue) {
								if (PhotonNetwork.isMasterClient) {
									ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
									hashTable.Add ("map", DataSerialize.Serialize (map));
									PhotonNetwork.room.SetCustomProperties (hashTable);
								}

								OnSpawnFound (new Vector3 ((float)i, 0.0f, (float)j));
							}
						}
					}
					else
					{
						if (PhotonNetwork.offlineMode) {
							for (int k = 0; k < TeamManager.RedTeam.Count; k++) {
								TeamManager.Spawn (TeamManager.RedTeam [k], new Vector3 ((float)i, 0.0f, (float)j));
							}
						} else {
							if (OnSpawnFound != null && PhotonNetwork.player.GetTeam () == PunTeams.Team.red) {
								if (PhotonNetwork.isMasterClient) {
									ExitGames.Client.Photon.Hashtable hashTable = new ExitGames.Client.Photon.Hashtable ();
									hashTable.Add ("map", DataSerialize.Serialize (map));
									PhotonNetwork.room.SetCustomProperties (hashTable);
								}

								OnSpawnFound (new Vector3 ((float)i, 0.0f, (float)j));
							}
						}
					}
				}
				else if (map[i, j] == Exit)
				{
					if (j < midPoint.z)
					{
						RedTeamExit.position = new Vector3((float)i, 0.0f, (float)j);
					}
					else
					{
						BlueTeamExit.position = new Vector3((float)i, 0.0f, (float)j);
					}
				}
			}
		}
	}
	
	
	void InitMap()
	{
		System.TimeSpan t = (System.DateTime.UtcNow - new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc));
        Random.InitState((int)t.TotalSeconds);
		float n = 0;
		
		for (int i = 1; i < rows - 1; i++)
		{
			for (int j = 1; j < cols - 1; j++)
			{
				n = Random.Range(0.0f, 100.0f);
				if (n < WALL_PCT)
				{
					map[i, j] = Wall;
				}
				else
				{
					map[i, j] = Floor;
				}
			}
		}
		
		for (int i = 0; i < rows; i++)
		{
			map[i, 0] = 1;
			map[i, cols - 1] = 1;
			
		}
		
		for (int j = 0; j < cols; j++)
		{
			map[0, j] = 1;
			map[(rows - 1), j] = 1;
		}
	}
	
	void GenerateMap()
	{
		int walls;
		
		for (int k = 0; k < 1; k++)
		{
			for (int i = 1; i < rows - 1; i++)
			{
				
				for (int j = 1; j < cols - 1; j++)
				{
					walls = WallsArround(map, i, j);
					
                    //Si un muro está rodeado de 4 o más muros, sigue siendo muro.
					if (map[i, j] == Wall && (walls >= 4))
					{
                        //map[i,j] = 1;
					}
					
                    //Si un suelo está rodeado de 5 o más muros, pasa a ser muro.
					else if (map[i, j] == Floor && (walls >= 5))
					{
						map[i, j] = Wall;
					}
					
                    //Si una casilla tiene uno o ningún muro a dos pasos, pasa a ser muro.
					else if (map[i, j] == Floor && (walls + WallsArround2(map, i, j)) <= 2)
					{
						map[i, j] = Wall;
					}
					
                    //En otro caso, la posición actual pasa a ser suelo.
					else if (map[i, j] != Floor)
					{
						map[i, j] = Floor;
					}
				}
			}
		}
		
		FreeCenter(MID_RADIUS, MIDWALL_SIZE);
		
        //Las siguientes iteraciones se realizan para suavizar el mapeado y eliminar
        //muros que puedan quedar sueltos en zonas abiertas.
		for (int k = 0; k < 2; k++)
		{
			for (int i = 1; i < rows - 1; i++)
			{
				for (int j = 1; j < cols - 1; j++)
				{
					walls = WallsArround(map, i, j);
                    //Si un muro está rodeado de ? o más muros, sigue siendo muro.
					if (map[i, j] == 1 && (walls >= 3))
					{
						map[i, j] = 1;
						
					}
					
                    //Si un suelo está rodeado de 5 o más muros, pasa a ser muro.
					else if (map[i, j] == 0 && (walls >= 5))
					{
						map[i, j] = 1;
					}
                    //En otro caso, la posición actual pasa a ser suelo.
					else
					{
						map[i, j] = 0;
					}
				}
			}
			
			if (k == 0)
			{
				MakeBottomPartRadialSymTo(map, map2);
				MakeTopPartRadialSym(map);
			}
		}
		
		for (int k = 0; k < 1; k++)
		{
			for (int i = 1; i < rows - 1; i++)
			{
				for (int j = 1; j < cols - 1; j++)
				{
					walls = WallsArround(map2, i, j);
                    //Si un muro está rodeado de ? o más muros, sigue siendo muro.
					if (map2[i, j] == 1 && (walls >= 3))
					{
						map2[i, j] = 1;
						
					}
					
                    //Si un suelo está rodeado de 5 o más muros, pasa a ser muro.
					else if (map2[i, j] == 0 && (walls >= 5))
					{
						map2[i, j] = 1;
					}
                    //En otro caso, la posición actual pasa a ser suelo.
					else
					{
						map2[i, j] = 0;
					}
				}
			}
		}
	}
	
	
	int WallsArround(int[,] m, int i, int j)
	{
		int wallCount = 0;
		
		if (m[(i - 1), (j - 1)] == 1) wallCount++;
        //else if(map[(i-1),(j-1)] == 2) map[(i-1),(j-1)] = 1;
		
		if (m[(i - 1), j] == 1) wallCount++;
		if (m[(i - 1), (j + 1)] == 1) wallCount++;
		
		if (m[i, (j - 1)] == 1) wallCount++;
		if (m[i, (j + 1)] == 1) wallCount++;
		
		if (m[(i + 1), (j - 1)] == 1) wallCount++;
		if (m[(i + 1), j] == 1) wallCount++;
		if (m[(i + 1), (j + 1)] == 1) wallCount++;
		
		return wallCount;
	}
	
	
	
	int WallsArround2(int[,] m, int i, int j)
	{
		int wallCount = 0;
		if (i != 1)
		{
			if (j != 1)
				if (m[(i - 2), (j - 2)] == 1) wallCount++;
			if (m[(i - 2), (j - 1)] == 1) wallCount++;
			if (m[(i - 2), j] == 1) wallCount++;
			if (m[(i - 2), (j + 1)] == 1) wallCount++;
			if (j != cols - 2)
				if (m[(i - 2), (j + 2)] == 1) wallCount++;
		}
		
		if (j != 1)
		{
			if (m[(i - 1), (j - 2)] == 1) wallCount++;
			if (m[i, (j - 2)] == 1) wallCount++;
			if (m[(i + 1), (j - 2)] == 1) wallCount++;
		}
		else if (j != cols - 2)
		{
			if (m[(i - 1), (j + 2)] == 1) wallCount++;
			if (m[i, (j + 2)] == 1) wallCount++;
			if (m[(i + 1), (j + 2)] == 1) wallCount++;
		}
		
		if (i != rows - 2)
		{
			if (j != 1)
				if (m[(i + 2), (j - 2)] == 1) wallCount++;
			if (m[(i + 2), (j - 1)] == 1) wallCount++;
			if (m[(i + 2), j] == 1) wallCount++;
			if (m[(i + 2), (j + 1)] == 1) wallCount++;
			if (j != cols - 2)
				if (m[(i + 2), (j + 2)] == 1) wallCount++;
		}
		
		return wallCount;
	}
	
	void MakeTopPartRadialSym(int[,] m)
	{
		int iT = 0;
		int iB = rows - 1;
		
		int jT = 0;
		int jB = cols - 1;
		
		while (iT < iB)
		{
			while (jT < cols)
			{
				m[iB, jB] = m[iT, jT];
				jT = jT + 1;
				jB = jB - 1;
			}
			
			jT = 0;
			jB = cols - 1;
			iT = iT + 1;
			iB = iB - 1;
		}
	}
	
	void MakeBottomPartRadialSymTo(int[,] m1, int[,] m2)
	{
		int iT = 0;
		int iB = rows - 1;
		
		int jT = 0;
		int jB = cols - 1;
		
		while (iT < iB)
		{
			while (jT < cols)
			{
				m2[iB, jB] = m1[iB, jB];
				m2[iT, jT] = m1[iB, jB];
				jT = jT + 1;
				jB = jB - 1;
			}
			
			jT = 0;
			jB = cols - 1;
			iT = iT + 1;
			iB = iB - 1;
		}
	}
	
	
	void FreeCenter(int radius, int halo)
	{
		int i;
		int j;
		float n;
		
		int half_rows = rows / 2;
		int half_cols = cols / 2;
		int par = 0;
		int impar = 0;
		if (rows % 2 == 0)
		{
			par = 1; impar = 0;
		}
		else
		{
			par = 0; impar = 1;
		}
		
		int distCenterRows = 0;
		int distCenterCols = 0;
		int dist = 0;
		halo = halo + radius;
		
		if (radius % 2 != rows % 2) radius++;
		
		for (i = 0; i < rows; i++)
		{
			for (j = 0; j < cols; j++)
			{
				distCenterRows = Mathf.Max(Mathf.Abs(i - half_rows) - par + impar, Mathf.Abs(i - half_rows + par) - par + impar);
				distCenterCols = Mathf.Max(Mathf.Abs(j - half_cols) - par + impar, Mathf.Abs(j - half_cols + par) - par + impar);
				dist = distCenterRows * distCenterRows + distCenterCols * distCenterCols;
				
				if (distCenterRows < radius && distCenterCols < radius && dist < radius * radius)
				{
					map[i, j] = 0;
				}
				else if (distCenterRows < halo && distCenterCols < halo && dist < halo * halo)
				{
					n = Random.Range(0.0f, 100.0f);
					if (n < MIDWALL_PCT) map[i, j] = Wall;
					else map[i, j] = Floor;
				}
			}
		}
	}
	
	
	int NumberWalkables(int[,] floor)
	{
		int i = 0;
		int j = 0;
		int walkables = 0;
		
		for (i = 0; i < rows; i++)
		{
			for (j = 0; j < cols; j++)
			{
				if (floor[i, j] == 0) walkables++;
			}
		}
		
		return walkables;
	}
	
	void IsConnected(int[,] floor, int[,] auxFloor, int x, int y)
	{
        //Si en el mapa tenemos suelo.
		if (floor[x, y] == 0)
		{
            //Y no lo hemos detectado aún como conexo en el auxiliar.
			if (auxFloor[x, y] != 0)
			{
                //Lo ponemos como conexo si alguno a su alrededor lo es.
				if ((auxFloor[x - 1, y] == 0) ||
				(auxFloor[x, y - 1] == 0) ||
				(auxFloor[x, y + 1] == 0) ||
				(auxFloor[x + 1, y] == 0))
				{
					auxFloor[x, y] = 0;
				}
			}
		}
	}
	
	int EnsureConnectivity(int[,] floor, int[,] auxFloor)
	{
		int i;
		int j;
		int x = rows / 2;
		int y = cols / 2;
		int nWalkable = 0;
		int nWalkableNew = 0;
		
		for (i = 0; i < rows; i++)
		{
			for (j = 0; j < cols; j++)
			{
				auxFloor[i, j] = Wall;
			}
		}
		
		
		auxFloor[x, y] = 0;
		
		
        //Recorremos el mapa en múltiples direcciones marcando como conexas
        //aquellas casillas que son suelo en el mapa principal y que tienen
        //a su alrededor alguna casilla marcada como conexa.
		do
		{
			nWalkable = nWalkableNew;
			for (i = 1; i < rows - 1; i++) for (j = 1; j < cols - 1; j++) IsConnected(floor, auxFloor, i, j);
			for (i = rows - 2; i > 0; i--) for (j = cols - 2; j > 0; j--) IsConnected(floor, auxFloor, i, j);
			for (i = rows - 2; i > 0; i--) for (j = 1; j < cols - 1; j++) IsConnected(floor, auxFloor, i, j);
			for (i = 1; i < rows - 1; i++) for (j = cols - 2; j > 0; j--) IsConnected(floor, auxFloor, i, j);
			nWalkableNew = NumberWalkables(auxFloor);
		}
			while (nWalkable != nWalkableNew);
		
		nWalkable = nWalkableNew;
		
        //Por último aplicamos los cambios al mapa principal.
		for (i = 1; i < rows - 1; ++i)
		{
			for (j = 1; j < cols - 1; ++j)
			{
                //Si en el mapa tenemos suelo y en el auxiliar lo tenemos como no conexo, lo convertimos en muro.
				if ((floor[i, j] == 0) && (auxFloor[i, j] == 1))
				{
					floor[i, j] = 1;
				}
			}
		}
		
		return nWalkable;
	}
	
	int dist(int X1, int Y1, int X2, int Y2)
	{
		int X = Mathf.Abs(X1 - X2);
		int Y = Mathf.Abs(Y1 - Y2);
		return X + Y;
	}
	
	int dist2(int X1, int Y1, int X2, int Y2)
	{
		return Mathf.Abs(Y1 - Y2);
	}
	
	int clamp(int v, int l, int h)
	{
		if (v < l) v = l;
		if (v > h) v = h;
		return v;
	}
	
	void FreeArround(int[,] m, int x, int y, int n)
	{
		for (int i = (-1) * (n - 1); i <= n - 1; i++)
		{
			for (int j = (-1) * (n - 1); j <= n - 1; j++)
			{
				m[x + i, y + j] = Floor;
			}
		}
		m[x - n, y] = Floor;
		m[x + n, y] = Floor;
		m[x, y + n] = Floor;
		m[x, y - n] = Floor;
	}
	
	void PlaceSpawnsAndExits(int[,] m)
	{
		int i = 0;
		int j = 0;
		int midX = rows / 2;
		int midY = cols / 2;
		
		int MD = 0, MD_X = 0, MD_Y = 0;
		int PMD = 0, PMD_X = 0, PMD_Y = 0;
		int D = 0;
		int DMD = 0;
		
		for (i = 0; i < midX; i++)
		{
			for (j = 0; j < cols; j++)
			{
				if (m[i, j] == 0)
				{
					D = dist(midX, midY, i, j);
					
					if (D > MD)
					{
						MD = D; MD_X = i; MD_Y = j;
					}
				}
			}
		}
		
		for (i = 0; (i < rows * 0.16 || PMD == 0) && i < midX; i++)
		{
			for (j = 0; j < cols; j++)
			{
				if (m[i, j] == 0)
				{
					D = dist(midX, midY, i, j);
					DMD = dist2(MD_X, MD_Y, i, j);
					
					if (D + DMD > PMD && DMD > cols * 0.6)
					{
						PMD = D + DMD; PMD_X = i; PMD_Y = j;
					}
				}
			}
		}
		
		int n = 3;
		
		MD_X = clamp(MD_X, n + 1, rows - 1 - (n + 1));
		MD_Y = clamp(MD_Y, n + 1, rows - 1 - (n + 1));
		PMD_X = clamp(PMD_X, n + 1, rows - 1 - (n + 1));
		PMD_Y = clamp(PMD_Y, n + 1, rows - 1 - (n + 1));
		
		FreeArround(m, MD_X, MD_Y, n);
		FreeArround(m, PMD_X, PMD_Y, n);
		
		m[MD_X, MD_Y] = Spawn;
		m[PMD_X, PMD_Y] = Exit;
	}
}
