﻿using UnityEngine;
using System.Collections;

public abstract class Character : MonoBehaviour
{
    public float attack { get; protected set; }
    public float defense { get; protected set; }
	public float walkSpeed { get; protected set; }
    public float runSpeed { get; protected set; }
    public float spawnTime { get; protected set; }

    public float special2Time { get; protected set; }
    public int special1Units { get; protected set; }
    public int special2Units { get; protected set; }
    public float special2Range { get; protected set; }
    
    public PlayerConfiguration playerConfig { get; private set; }

    private bool isSpecial2Active = false;
    private int currentUnits1;
    private int currentUnits2;
    private bool special2Available;

    private UnityEngine.UI.Text special1Text;
	private UnityEngine.UI.Text special2Text;
	
	private UnityEngine.UI.Image special1Image;
	private UnityEngine.UI.Image special2Image;

    void Start()
    {
		playerConfig = gameObject.GetComponentInParent<PlayerConfiguration>();
	    currentUnits1 = special1Units;
	    currentUnits2 = special2Units;
	    
	    special1Text = GameObject.Find("Special1Text" + (int)playerConfig.playerNumber).GetComponent<UnityEngine.UI.Text>();
	    special2Text = GameObject.Find("Special2Text" + (int)playerConfig.playerNumber).GetComponent<UnityEngine.UI.Text>();
	    
	    special1Image = GameObject.Find("Special1Image" + (int)playerConfig.playerNumber).GetComponent<UnityEngine.UI.Image>();
	    special2Image = GameObject.Find("Special2Image" + (int)playerConfig.playerNumber).GetComponent<UnityEngine.UI.Image>();
	    
	    special1Text.text = special1Units.ToString();
	    special2Text.text = special2Units.ToString();
    }
    
	void Update ()
    {
        castPassive();

        special1Text.text = currentUnits1.ToString();
        special2Text.text = currentUnits2.ToString();

        if (playerConfig.GetComponent<PlayerStatus>().GetCurrentHealth() > 0)
		{
			if (playerConfig.userControl.isSpecial1() && currentUnits1 > 0)
            {
				currentUnits1--;
				special1Text.text = currentUnits1.ToString();
				if(currentUnits1 <= 0)
					special1Image.color = Color.grey;
                castSpecial1();
            }
			else if (playerConfig.userControl.isSpecial2() && !isSpecial2Active && currentUnits2 > 0)
				StartCoroutine(Special2(gameObject));
		}
	}

    IEnumerator Special2(GameObject owner)
    {
	    isSpecial2Active = true;
	    currentUnits2 --;
	    if(currentUnits2 <= 0)
		    special2Image.color = Color.grey;
	    special2Text.text = currentUnits2.ToString();
        castSpecial2();
        yield return new WaitForSeconds(special2Time);
        stopSpecial2();
        isSpecial2Active = false;
    }

    public void ReloadSpecial1()
    {
        currentUnits1 = special1Units;
        special1Text.text = currentUnits1.ToString();
        special1Image.color = Color.white;
    }

    public void ReloadSpecial2()
    {
        currentUnits2 = special2Units;
        special2Text.text = currentUnits2.ToString();
        special2Image.color = Color.white;
    }

	public void IncreaseHealth()
	{
		defense = 150;
	}

	public void increaseDamage(float amount)
	{
		attack += amount;
	}

    public abstract void castSpecial1();
    public abstract void castSpecial2();
    public abstract void stopSpecial2();
    public abstract void castPassive();
}
