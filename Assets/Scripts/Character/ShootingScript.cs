﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootingScript : MonoBehaviour
{		
    [SerializeField]
	public ParticleSystem muzzleFlash;
    [SerializeField]
	public GameObject impactPrefab;
    [SerializeField]
	public int maxImpacts;
    [SerializeField]
	public float shootingRate;
    [SerializeField]
	public LayerMask mask;
    [SerializeField]
	public Transform origin;
    [SerializeField]
	public int maxBullets;
    [SerializeField]
	public float bulletsRange;
    [SerializeField]
	public float bulletErrorWalking;
    [SerializeField]
	public float bulletErrorRunning;

    // Managers
    private PlayerConfiguration playerConfig;
    private NotificationSystem notificationSystem;
    
    private Text ammoText;
    private Text reloadingText;
    private float currentKills;
    private Text killsText;
    private GameObject scorePanel;
    private CharacterSounds charSounds;
    private PlayerStatus playerStatus;

    private Animator playerAnimator;
    private GameObject[] impacts;
    private int currentImpact = 0;
    private int currentBullets;
    private bool shooting = false;
    private bool reloading = false;
    private float waitTilNextFire = 0.0f;
    private float shootingDesviation = 0.0f;
    private Vector3 crosshairPos;
    
    void Start ()
    {
        SetupLocalVariables();
        SetupComponents();
        SetupUI();
        SetupBulletHoles();
        setCrosshairPos();
	}

    private void SetupBulletHoles()
    {
        impacts = new GameObject[maxImpacts];
        for (int i = 0; i < maxImpacts; i++)
        {
            impacts[i] = (GameObject)Instantiate(impactPrefab);
            impacts[i].transform.SetParent(GameObject.FindGameObjectWithTag("BulletHole").transform);
        }
    }

    private void SetupComponents()
    {
        playerStatus = GetComponent<PlayerStatus>();
        charSounds = gameObject.GetComponentInParent<CharacterSounds>();
        playerConfig = gameObject.GetComponentInParent<PlayerConfiguration>();
        playerAnimator = GetComponent<Animator>();
    }

    private void SetupLocalVariables()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        currentKills = 0;
        currentBullets = maxBullets;
    }

    private void SetupUI()
    {
        notificationSystem = GameObject.Find("NotificationSystem").GetComponent<NotificationSystem>();
        killsText = GameObject.Find("KillsText" + (int)playerConfig.playerNumber).GetComponent<Text>();
        reloadingText = GameObject.Find("ReloadingText" + (int)playerConfig.playerNumber).GetComponent<Text>();
        ammoText = GameObject.Find("AmmoText" + (int)playerConfig.playerNumber).GetComponent<Text>();
        scorePanel = GameObject.Find("ScorePanel" + (int)playerConfig.playerNumber);

        reloadingText.gameObject.SetActive(false);

        ammoText.text = currentBullets.ToString();
        killsText.text = currentKills.ToString();
    }
	
	void Update () 
	{
		if (playerConfig.userControl.isShooting() &&  waitTilNextFire <= 0 && !playerConfig.userControl.isRunning() && !playerStatus.settingsPanelOpened)
		{
			shooting = true;
		}

        if(playerConfig.userControl.isReloading())
        {
            StartCoroutine("Reload");
        }

        if(playerConfig.userControl.isInfo())
        {
            scorePanel.gameObject.SetActive(true);
        }
        else
        {
            scorePanel.gameObject.SetActive(false);
        }
		
		waitTilNextFire -= Time.deltaTime * shootingRate;
	}
	
	
	void FixedUpdate()
	{
		Shoot ();
	}

	public void Shoot()
	{
		if (shooting && GetComponent<PlayerStatus>().GetCurrentHealth() > 0 && !reloading)
		{
			currentBullets--;
			ammoText.text = currentBullets.ToString();

			if (currentBullets == 0)
			{
				StartCoroutine("Reload");
			}

			waitTilNextFire = 1;
			muzzleFlash.Clear();
			muzzleFlash.Simulate(muzzleFlash.duration);
			muzzleFlash.Play();

			charSounds.shootClip.PlayOneShot(charSounds.shootClip.clip);

			shooting = false;

			RaycastHit hit;
			Vector3 dir;

			Ray ray;
			Vector3 shootingOrigin = origin.position;

			if (playerConfig.userControl.isMoving())
				shootingDesviation = bulletErrorRunning;
			else
				shootingDesviation = bulletErrorWalking;

			ray = playerConfig.cam.ScreenPointToRay(crosshairPos);

			if (Physics.Raycast(ray, out hit, 100.0f, mask))
			{
				dir = (hit.point - shootingOrigin).normalized;
			}
			else
			{
				dir = ray.direction;
			}

			if (Physics.Raycast(shootingOrigin, dir, out hit, bulletsRange))
			{
				Quaternion hitRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);

				if (hit.transform.tag == "Prop")
				{
					float zFx = 0.01f, zFz = 0.01f;
					if (hit.normal.x < 0) zFx = -0.01f;
					if (hit.normal.z < 0) zFz = -0.01f;

					impacts[currentImpact].transform.position = new Vector3(hit.point.x + zFx, hit.point.y, hit.point.z + zFz);
					impacts[currentImpact].transform.rotation = hitRotation;
				}
				else if (hit.transform.tag == "Player" && hit.transform.gameObject.GetComponent<PlayerConfiguration>().team != playerConfig.team)
				{
					if (!hit.collider.isTrigger) {
                        HitPlayerWithAttackMultiplier(hit, 1.0f);
						
					} else {
                        HitPlayerWithAttackMultiplier(hit, 2.0f);
                    }

					if (hit.transform.gameObject.GetComponent<PlayerStatus>().GetCurrentHealth() <= 0) {
						if (playerConfig.playerClass == PlayerConfiguration.PlayerClass.Mercenary) {
							playerConfig.characterClass.GetComponent<Character> ().increaseDamage (10);
						}

						currentKills++;
						charSounds.killClip.Play();
						killsText.text = currentKills.ToString();
						notificationSystem.OpenNotificationPanelWithText((int)playerConfig.playerNumber - 1, "You killed a player");
					}
				}

				if (++currentImpact >= maxImpacts)
				{
					currentImpact = 0;
				}
			}
		}
	}

    private void HitPlayerWithAttackMultiplier(RaycastHit hit, float multiplier)
    {
        if (PhotonNetwork.offlineMode)
        {
            hit.transform.gameObject.GetComponent<PlayerStatus>().Hit(GetComponent<Character>().attack * multiplier);
        }
        else
        {
            hit.transform.GetComponentInParent<PhotonView>().RPC("Hit", PhotonTargets.All, GetComponent<Character>().attack * multiplier);
        }
    }

    IEnumerator Reload()
    {
        charSounds.reloadClip.Play();
        reloading = true;
        reloadingText.gameObject.SetActive(true);
        yield return new WaitForSeconds(2);
        currentBullets = maxBullets;
		ammoText.text = currentBullets.ToString();
        reloadingText.gameObject.SetActive(false);
        reloading = false;
    }

    void setCrosshairPos()
    {
		if (Global.numberLocalPlayers == 1) {
			if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Single)
				crosshairPos = new Vector3 (Screen.width / 2, Screen.height / 2, 0.0f);
		} else if (Global.numberLocalPlayers == 2) {
			if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split1)
				crosshairPos = new Vector3 (Screen.width / 4, Screen.height / 2, 0.0f);
			else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split2)
				crosshairPos = new Vector3 (Screen.width - Screen.width / 4, Screen.height / 2, 0.0f);
		} else if (Global.numberLocalPlayers == 4) {
			if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split1)
				crosshairPos = new Vector3 (Screen.width / 4, Screen.height - Screen.height / 4, 0.0f);
			else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split2)
				crosshairPos = new Vector3 (Screen.width - Screen.width / 4, Screen.height - Screen.height / 4, 0.0f);
			else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split3)
				crosshairPos = new Vector3 (Screen.width / 4, Screen.height / 4, 0.0f);
			else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split4)
				crosshairPos = new Vector3 (Screen.width - Screen.width / 4, Screen.height / 4, 0.0f);
		} else {
			if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Single)
				crosshairPos = new Vector3(Screen.width / 2, Screen.height / 2, 0.0f);
		}
    }
}