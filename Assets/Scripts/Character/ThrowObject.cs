﻿using UnityEngine;
using System.Collections;

public class ThrowObject : MonoBehaviour {

    public enum GrenadeType { Explosive, Smoke };
    private PlayerConfiguration playerConfig;
    public Transform origin;

	Vector3 crosshairPos;

    void Start()
    {
		playerConfig = gameObject.GetComponentInParent<PlayerConfiguration>();

		if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Single)
			crosshairPos = new Vector3(Screen.width / 2, Screen.height / 2, 0.0f);
		else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split1)
			crosshairPos = new Vector3(Screen.width / 4, Screen.height - Screen.height / 4, 0.0f);
		else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split2)
			crosshairPos = new Vector3(Screen.width - Screen.width / 4, Screen.height - Screen.height / 4, 0.0f);
		else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split3)
			crosshairPos = new Vector3(Screen.width / 4, Screen.height / 4, 0.0f);
		else if (playerConfig.playerNumber == PlayerConfiguration.PlayerNumber.Split4)
			crosshairPos = new Vector3(Screen.width - Screen.width / 4, Screen.height / 4, 0.0f);
    }

	public void Throw(GameObject throwable, float force, GrenadeType type)
    {
        RaycastHit hit;
        Vector3 dir;

        Ray ray;

        ray = playerConfig.cam.ScreenPointToRay(crosshairPos);

        if (Physics.Raycast(ray, out hit, 50.0f))
        {
            dir = (hit.point - transform.position).normalized;
        }
        else
        {
            dir = ray.direction;
        }


		GameObject grenade;
		if (PhotonNetwork.offlineMode) {
			grenade = GameObject.Instantiate(throwable, origin.position, origin.rotation) as GameObject;
		} else {
			grenade = PhotonNetwork.Instantiate (throwable.name, origin.position, origin.rotation, 0);
		}

        grenade.GetComponent<Rigidbody>().AddForce(dir * force, ForceMode.Impulse);

    }
}
