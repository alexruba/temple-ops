﻿using UnityEngine;
using System.Collections;

public class CharacterSounds : MonoBehaviour
{
    public AudioSource leftFootStepClip;
    public AudioSource rightFootStepClip;
    public AudioSource shootClip;
    public AudioSource reloadClip;
	public AudioSource deadClip;
	public AudioSource hitClip;
	public AudioSource killClip;

    public void LeftFootStepSoundPlay()
    {
        leftFootStepClip.Play();
    }

    public void RightFootStepSoundPlay()
    {
        rightFootStepClip.Play();
    }
}
