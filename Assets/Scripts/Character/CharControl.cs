﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharControl : MonoBehaviour
{
	private PlayerConfiguration playerConfig;
	private MouseAimCamera camera;
	
	//Rigidbody m_Rigidbody;
	Animator m_Animator;
	//bool m_IsGrounded;
    bool resting = false;

    private float playerSpeed;
    public float walkingSpeed = 5f;
	public float runningSpeed = 10f;
    public float maxStamina;
	private float currentStamina;
	private Slider staminaSlider;
    public float jumpSpeed = 5.0f;
    public float gravity = -9.81f;

    private float playerRotation;
	private CharacterController charControl;
	private Vector3 moveVector;

	private float ySpeed;
	
	private float shootLookTime;
	private float shootLookTimeReset = 1.0f;

    // Use this for initialization
    void Start ()
	{ 
		playerConfig = gameObject.GetComponent<PlayerConfiguration>();
		camera = playerConfig.cam.GetComponent<MouseAimCamera>();

		walkingSpeed = GetComponent<Character> ().walkSpeed;
		runningSpeed = GetComponent<Character> ().runSpeed;
		m_Animator = GetComponentInChildren<Animator>();
		//m_Rigidbody = GetComponent<Rigidbody>();
		
        charControl = gameObject.GetComponent<CharacterController>();
        playerRotation = 0.0f;
		//m_IsGrounded = true;
		currentStamina = maxStamina;
		// UI Setup
		staminaSlider = GameObject.Find("StaminaSlider" + (int)playerConfig.playerNumber).GetComponent<Slider>();
		staminaSlider.value = currentStamina;
		staminaSlider.maxValue = maxStamina;
		
		shootLookTime = 0.0f;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		moveVector = new Vector3(0.0f, 0.0f, 0.0f);
		playerRotation = transform.eulerAngles.y;

		if (gameObject.GetComponent<PlayerStatus> ().GetCurrentHealth () > 0)
		{
			if (playerConfig.userControl.isMoving ())
			{
				moveVector = new Vector3 (playerConfig.userControl.getMoveX (), 0.0f, playerConfig.userControl.getMoveY ());
				//Debug.Log("X: " + playerConfig.userControl.getMoveX () + "  Y: " + playerConfig.userControl.getMoveY ());
			}

			// FIRST PERSON CAMERA
			if (camera.isFirstPersonCamera ())
			{
				playerRotation = camera.getCamRotation ();
			}
			// THIRD PERSON CAMERA
			else if (camera.isThirdPersonCamera ())
			{
				// Si disparamos o apuntamos, miramos al mismo sitio que la cámara
				shootLookTime -= Time.deltaTime;
				if (shootLookTime > 0.0f || playerConfig.userControl.isAiming ())
				{
					playerRotation = camera.getCamRotation ();
				}

				if (playerConfig.userControl.isShooting ())
				{
					shootLookTime = shootLookTimeReset;
				}
			        
				if (playerConfig.userControl.isMoving ())
				{
					// Si no disparamos, y tampoco apuntamos
					if (!playerConfig.userControl.isAiming () && !playerConfig.userControl.isShooting () && shootLookTime <= 0.0f)
					{
						//Mathf.Deg2Rad;
						//Debug.Log("AsinX: " + (Mathf.Asin(moveVector.normalized.x) * Mathf.Rad2Deg) + " AcosX: " + (Mathf.Acos(moveVector.normalized.x) * Mathf.Rad2Deg) + " AsinZ: " + (Mathf.Asin(moveVector.normalized.z) * Mathf.Rad2Deg) + " AcosZ: " + (Mathf.Acos(moveVector.normalized.z) * Mathf.Rad2Deg));
						//Debug.Log(Mathf.Asin(moveVector.normalized.z) * Mathf.Rad2Deg);
						//Debug.Log(Mathf.Acos(moveVector.normalized.y) * Mathf.Rad2Deg);

						// Definimos en asinx la diferencia de rotación respecto a la cámara
						float asinx = Mathf.Asin (moveVector.normalized.x) * Mathf.Rad2Deg;
						float asinz = Mathf.Asin (moveVector.normalized.z) * Mathf.Rad2Deg;

						if (asinz < 0)
						{
							float signo = 1;
							if (asinx != 0)
								signo = (asinx / Mathf.Abs (asinx));
							asinx = (asinx - signo * 180) * (-1);
						}

						// Rotación
						playerRotation = camera.getCamRotation () + asinx;
						//Debug.Log("playerRotation: " + playerRotation + " cam: " + playerConfig.cam.transform.eulerAngles.y + "  asinx: " + asinx);
					}
				}
			}

			// Gravedad
			if (charControl.isGrounded)
			{
				if (playerConfig.userControl.isJumping ())
				{
					m_Animator.SetTrigger ("Jump");
					ySpeed = jumpSpeed;
				} else
				{
					ySpeed = 0.0f;
				}
			}
			ySpeed += gravity * Time.deltaTime;
			moveVector.y += ySpeed;

			Vector3 move;

			if (currentStamina <= 0 && !resting)
				StartCoroutine ("Rest");
			else if (!resting)
			{
				if (playerConfig.userControl.isRunning () && !resting)
				{
					playerSpeed = runningSpeed;
					currentStamina--;
				} else
				{
					playerSpeed = walkingSpeed;
					if (currentStamina < maxStamina)
						currentStamina++;
				}
			}
	        
			staminaSlider.value = currentStamina;

			move = (Quaternion.Euler (0, camera.getCamRotation (), 0) * moveVector * playerSpeed * Time.deltaTime);
			charControl.Move (move);
			transform.rotation = Quaternion.Euler (0, playerRotation, 0);
			//UpdateAnimator(move);
			Vector3 movementXZ = new Vector3 (move.x, 0.0f, move.z);
			float movement_Amount = movementXZ.magnitude * 25.0f;
			m_Animator.SetFloat ("Speed", movement_Amount, 0.1f, Time.deltaTime);
		}
	}

    IEnumerator Rest()
    {
        resting = true;
        playerSpeed = walkingSpeed;
        yield return new WaitForSeconds(1.5f);
        currentStamina+=maxStamina/2;
        resting = false;
    }
	
	public float getCharRotation()
	{
		return playerRotation;
	}
	
	/*void UpdateAnimator(Vector3 move)
	{
			// update the animator parameters
		Debug.Log(move.magnitude);
		float m_ForwardAmount = move.magnitude * 25.0f;
		if(move.magnitude < 0.015f) m_ForwardAmount = 0.0f;
		m_Animator.SetFloat("Forward", m_ForwardAmount, 0.1f, Time.deltaTime);
		//m_Animator.SetFloat("Turn", m_TurnAmount, 0.1f, Time.deltaTime);
		m_Animator.SetBool("OnGround", m_IsGrounded);
		if (!m_IsGrounded)
		{
			m_Animator.SetFloat("Jump", m_Rigidbody.velocity.y);
		}
		
			// calculate which leg is behind, so as to leave that leg trailing in the jump animation
			// (This code is reliant on the specific run cycle offset in our animations,
			// and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
		float runCycle =
			Mathf.Repeat(
			m_Animator.GetCurrentAnimatorStateInfo(0).normalizedTime + 0.2f, 1);
		float jumpLeg = (runCycle < 0.9f ? 1 : -1) * m_ForwardAmount;
		if (m_IsGrounded)
		{
			m_Animator.SetFloat("JumpLeg", jumpLeg);
		}
		
			// the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
			// which affects the movement speed because of the root motion.
		//if (charControl.isGrounded && move.magnitude > 0)
		//{
		//	m_Animator.speed = playerSpeed;
	//}
		//else
		//{
				// don't use that while airborne
			m_Animator.speed = 1;
	//}
	}*/
}
