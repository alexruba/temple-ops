﻿using UnityEngine;
using System.Collections;

public interface ICharacter
{
	void Passive (GameObject owner, int team);

	void Special1 (GameObject owner);

	void Special2 (GameObject owner);

    void StopSpecial2(GameObject owner);
}