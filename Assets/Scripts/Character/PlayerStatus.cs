using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerStatus : MonoBehaviour {
	
	private PlayerConfiguration playerConfig;

	private float currentHealth;
	public ParticleSystem damageParticles;
	private Text healthText;
	private Text teleportText;
	private GameObject deadPanel;
    public GameObject treasure;
	private Text deathsText;
	private GameObject damageImage;
    private float currentDeaths;
	private CharacterSounds charSounds;

	private GameObject treasureImage;
	
	public Vector3 spawnPoint;
	private bool spawning;
    public bool settingsPanelOpened = false;

    Animator playerAnimator;
    PhotonView photonView;
    
    void Start () 
	{
        SetupComponents();
        SetupLocalVariables();
        SetupUI();
	}

    private void SetupLocalVariables()
    {
        currentHealth = playerConfig.characterClass.GetComponent<Character>().defense;
        currentDeaths = 0;
        spawnPoint = new Vector3(transform.position.x, transform.position.y, transform.position.z);
    }

    private void SetupUI()
    {
        healthText = GameObject.Find("HealthText" + (int)playerConfig.playerNumber).GetComponent<Text>();
        teleportText = GameObject.Find("TeleportText" + (int)playerConfig.playerNumber).GetComponent<Text>();
        deadPanel = GameObject.Find("DeadPanel" + (int)playerConfig.playerNumber);
        deathsText = GameObject.Find ("DeathsText" + (int)playerConfig.playerNumber).GetComponent<Text> ();
        damageImage = GameObject.Find("DamageImage" + (int)playerConfig.playerNumber);
        treasureImage = GameObject.Find("TreasureImage" + (int)playerConfig.playerNumber);

        healthText.text = currentHealth.ToString();
        deathsText.text = currentDeaths.ToString();
        damageImage.SetActive(false);
        treasureImage.SetActive(false);
        deadPanel.SetActive(false);
    }

    private void SetupComponents()
    {
        charSounds = GetComponent<CharacterSounds>();
        playerConfig = GetComponent<PlayerConfiguration>();
        playerAnimator = gameObject.GetComponentInChildren<Animator>();

        photonView = GetComponent<PhotonView>();
    }
	
	void Update()
	{
        // Settings Panel
        if (settingsPanelOpened)
        {
            if (playerConfig.userControl.isPause())
            {
                GameObject.Find("Canvas").GetComponent<Menu>().CloseSettings();
                settingsPanelOpened = false;
            }
            else if(playerConfig.userControl.isInfo())
            {
                GameObject.Find("Canvas").GetComponent<LoadingScreen>().OnLevelLoad("Menu");
            }
        }
        else
        {
            if (playerConfig.userControl.isPause() && !settingsPanelOpened)
            {
                GameObject.Find("Canvas").GetComponent<Menu>().OpenSettings();
                settingsPanelOpened = true;
            }
        }

        
		if (currentHealth <= 0)
		{
            if (!playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Die")) {
                TriggerDieAnimation();
            }
			
			if(spawning)
			{
				if (playerConfig.userControl.isJumping())
					SpawnOnTeleport();
			}
		}

        if (playerConfig.HasTreasure)
            treasureImage.SetActive(true);
		else
            treasureImage.SetActive(false);
	}
	
	public float GetCurrentHealth()
	{
		return currentHealth;
	}

    public void Heal(float amount)
    {
        currentHealth += amount;
        healthText.text = currentHealth.ToString();
    }

	[PunRPC]
	public void Hit(float damage)
	{
		damageParticles.Clear();
		damageParticles.Simulate(damageParticles.duration);
		damageParticles.Play();
		
		currentHealth -= damage;
		
		if (currentHealth <= 0)
		{
            charSounds.deadClip.Play();
            healthText.text = "0";
            currentDeaths++;
            deathsText.text = currentDeaths.ToString();

            if (playerConfig.HasTreasure)
            {
				if (PhotonNetwork.offlineMode) {
					GameObject.Instantiate(treasure, new Vector3(transform.position.x, 0.2f, transform.position.z), Quaternion.identity);
				} else {
					PhotonNetwork.Instantiate ("Treasure", new Vector3 (transform.position.x, 0.2f, transform.position.z), Quaternion.identity, 0);
				}

                playerConfig.HasTreasure = false;
            }

            if (TeleportSystem.ExistsTeleportInTeam((int)playerConfig.team))
            {
                spawning = true;
                teleportText.text = "Teleport is ready. Press A or Space to use it";
            }
            else
            {
                teleportText.text = "No teleport available";
            }

            deadPanel.SetActive(true);

            StartCoroutine("SpawnInTime");
		}
		else
		{
			charSounds.hitClip.Play();
			StartCoroutine("DamageImage");
			healthText.text = currentHealth.ToString();
		}

    }
	
	public void Spawn()
	{
		StopCoroutine("SpawnInTime");
        deadPanel.SetActive(false);

		gameObject.transform.position = spawnPoint;

        ResetRotation();
        TriggerResurrectAnimation();

        gameObject.GetComponent<Character>().ReloadSpecial1();
		currentHealth = GetComponent<Character>().defense;
        healthText.text = currentHealth.ToString();
	}
	
	public void SpawnOnTeleport()
	{
		StopCoroutine("SpawnInTime");
        deadPanel.SetActive(false);

        ResetRotation();

        gameObject.transform.position = TeleportSystem.UseTeleport((int)playerConfig.team);
        TriggerResurrectAnimation();

        gameObject.GetComponent<Character>().ReloadSpecial1();
        currentHealth = GetComponent<Character>().defense;
        healthText.text = currentHealth.ToString();
	}

    private void ResetRotation()
    {
        gameObject.transform.rotation = Quaternion.identity;
        gameObject.transform.localRotation = Quaternion.identity;
    }

    private void TriggerDieAnimation()
    {
        playerAnimator.SetTrigger("Die");

        if (!PhotonNetwork.offlineMode)
        {
            GameObject.Find("GameplayNetwork").GetComponent<PhotonView>().RPC("RPC_PlayerDieAnimation", PhotonTargets.All, photonView.viewID);
        }
    }

    private void TriggerResurrectAnimation()
    {
        playerAnimator.SetTrigger("Resurrect");

        if (!PhotonNetwork.offlineMode)
        {
            GameObject.Find("GameplayNetwork").GetComponent<PhotonView>().RPC("RPC_PlayerResurrectAnimation", PhotonTargets.All, photonView.viewID);
        }
    }
	
	private IEnumerator SpawnInTime()
	{
		yield return new WaitForSeconds(GetComponent<Character>().spawnTime);
		spawning = false;
		Spawn();
	}
	
	IEnumerator DamageImage()
	{
		damageImage.SetActive(true);
		yield return new WaitForSeconds(0.2f);
		damageImage.SetActive(false);
	}
}
