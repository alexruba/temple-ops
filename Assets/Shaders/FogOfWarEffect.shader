Shader "Hidden/FogOfWar"
{
    Properties
    {
        _MainTex ("-", 2D) = ""{}
    }

    CGINCLUDE

    #include "UnityCG.cginc"

    sampler2D _MainTex;
    sampler2D_float _CameraDepthTexture;
    float4x4 _InverseView;

    sampler2D _FogMask;
    sampler2D _MapMask;
    float3 _FogMappingMin;
    float3 _FogMappingMax;
    half _FogScattering;
    float4 _FogColor;

    float3 GetWorldCoords(float2 uv)
    {
        // Get linear depth from Camera's depth texture.
        float depth = LinearEyeDepth(SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, float2(uv.x, 1.0 - uv.y)));

        // Get frustrum derivatives over X and Y w.r.t depth and determine view position.
        float2 ddxy = float2(unity_CameraProjection._11, unity_CameraProjection._22);
        float3 viewCoords = float3((uv * 2.0 - 1.0) / ddxy, -1) * depth;

        // Use the inverse transform matrix to figure out the world coordinates.
        return mul(_InverseView, float4(viewCoords, 1)).xyz;
    }

    float2 GetMaskCoords(float3 pos)
    {
        return (pos.xz - _FogMappingMin.xz) / (_FogMappingMax.xz - _FogMappingMin.xz);
    }

    float4 frag (v2f_img IN) : SV_Target
    {
        float4 color = tex2D(_MainTex, IN.uv);

        float3 worldCoords = GetWorldCoords(IN.uv);

        // Volumetric Shadow Mapping -- Raymarch from current camera position 
        // and determine fog scattering from the mask texture.

        const int MaxSteps = 1500;

        // We start from the camera to the fragment.

        float2 startCoords = GetMaskCoords(_WorldSpaceCameraPos);
        float2 endCoords = GetMaskCoords(worldCoords);
        float2 coords = startCoords;

        // Determine distance, step size and increment.

        float dist = distance(startCoords, endCoords);
        float stepSize = dist / MaxSteps;
        float2 coordsInc = normalize(endCoords - startCoords) * stepSize;

        // Compute scattering.

        float scattering = 0.0;

        for (int i = 0; i < MaxSteps; i++)
        {
        	//scattering += stepSize * (1.0 - tex2D(_FogMask, coords).a);
        	
        	if(tex2D(_FogMask, coords).a ==  0)// && tex2D(_MapMask, coords).a != 0)
        	{
        		scattering += stepSize * (tex2D(_MapMask, coords).a);
        	}
        	else if(tex2D(_FogMask, coords).a ==  1)
        	{
        		scattering += 0.0;
        	}
        	else
        	{
        		if(tex2D(_MapMask, coords).a == 1)
        		{
        			scattering += stepSize * (1.0 - tex2D(_FogMask, coords).a);
        		}
        		scattering += 0.0;
        	}
        	
            coords += coordsInc;
        }

        // Compute light scattering using Beer-Lambert's law.
        // (Or something similar).

        scattering = exp(-scattering * _FogScattering);
        scattering = 1.0 - saturate(scattering);

        // Compute final color!

        return lerp(color, _FogColor, scattering);
    }

    ENDCG

    SubShader
    {
        Cull Off ZWrite Off ZTest Always
        Pass
        {
            CGPROGRAM
            #pragma vertex vert_img
            #pragma fragment frag
            ENDCG
        }
    }
}
