﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class FogOfWarEffect : MonoBehaviour
{
	[Range(0, 100)] public float fogScattering = 0.5f;
    public Shader shader;
    public Color fogColor;
	//public Texture2D fogMask;
    public Vector3 mappingMinCoords;
    public Vector3 mappingMaxCoords;
	
	private Texture2D fogMask;
	private Texture2D mapMask;
    private Camera camera;
    private Material material;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (camera == null)
        {
            camera = GetComponent<Camera>();
            camera.depthTextureMode = DepthTextureMode.Depth;
        }

        if (material == null)
        {
            material = new Material(shader);
            material.hideFlags = HideFlags.DontSave;
        }
	    
	    fogMask = camera.transform.parent.GetComponentInChildren<Minimap>().getMinimapTex();
	    mapMask = Global.mapFogTexture;
	    

        material.SetMatrix("_InverseView", camera.cameraToWorldMatrix);
        material.SetFloat("_FogScattering", fogScattering);
        material.SetVector("_FogMappingMin", mappingMinCoords);
        material.SetVector("_FogMappingMax", mappingMaxCoords);
	    material.SetTexture("_FogMask", fogMask);
	    material.SetTexture("_MapMask", mapMask);
        material.SetColor("_FogColor", fogColor);

        Graphics.Blit(source, destination, material);
    }
}